<%-- 
    Document   : Add Vehicle
    Created on : Nov 17, 2022, 12:31:08 AM
    Author     : khang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add vehicle page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    </head>
    <body>


        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div> 

        <form action="ServletDispatcher" method="POST"
              style="background-color: #ffffff; display: inline-block; 
              border: #000; width: 30%; margin-left: 35%">
            <div style="text-align: center; background-color: #8223E7; color: #ffffff;  height: 90px;  padding-top: 30px">

                <b style="font-size: 50px;">              
                    Create new Vehicle
                </b>
            </div>

            <div style="padding-left: 50px; padding-bottom: 30px">
                <div style="padding-top: 20px; padding-bottom: 10px">
                    <b style="font-weight: bold; font-size: 20px">
                        Vehicle name
                    </b>
                    <input type="text" id="txtLicense" name="txtVehicleName" required="required" 
                           style="background-color: #EEEEEE; border: none; 
                           margin-bottom: 10px; margin-top: 10px"/>
                    <h3>${requestScope.Error.vehicleNameErrorMsg}</h3>
                </div>

                <div class="">
                    <b style="font-weight: bold; font-size: 20px">
                        License Flate
                    </b><br/>
                    <input type="text" id="txtLicense" name="txtLicense" required="required" 
                           style="background-color: #EEEEEE; border: none; 
                           margin-bottom: 10px; margin-top: 10px"/>
                    <h3>${requestScope.Error.licenseFlateErrorMsg}</h3>
                </div>

                <div style="padding-top: 20px">
                    <b style="font-weight: bold; font-size: 20px">
                        Car type
                    </b>
                    <select name="vehicleType" style="margin-left: 10px; padding-left: 50px">
                        <c:forEach var="vehicleType" items="${applicationScope.LIST_TYPE_CAR}">
                            <option value="${vehicleType.carTypeName}" > ${vehicleType.carTypeName}</option>
                        </c:forEach>                            
                    </select>
                </div>

                <div style="padding-top: 20px">
                    <b style="font-weight: bold; font-size: 20px">
                        Number of seat
                    </b>
                    <select name="numberSeat" style="margin-left: 10px; padding-left: 50px">
                        <c:forEach var="seat" items="${applicationScope.LIST_TYPE_CAR}">
                            <option value="${seat.numberOfSeat}" > ${seat.numberOfSeat}</option>
                        </c:forEach>                            
                    </select>
                </div>
            </div>
            <div style="text-align: center; padding-bottom: 20px; border: none">
                <button type="submit" value="CreateVehicle" name="btnAction"
                        style="background-color: #04AA6D; color: #ffffff; padding: 14px 20px; border: none">
                    <b style="font-weight: bold">
                        Create Vehicle
                    </b>
                </button>
            </div>
        </form>
        <div style="text-align: center; padding-bottom: 20px; color: red;">
            <b style="font-weight: bold">
                ${requestScope.Success}
            </b>      
        </div>
    </body>
</html>
