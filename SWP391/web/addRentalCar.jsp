<%-- 
    Document   : addRentalCar
    Created on : Nov 17, 2022, 8:18:25 PM
    Author     : KenLy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add rental car page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link href="css/Addtrip.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div> 
        
        <form action="ServletDispatcher" method="POST"
               style="background-color: #ffffff; display: inline-block; 
               border: #000; width: 30%; margin-left: 35%">
            <div>
                <div style="text-align: center; background-color: #8223E7; color: #ffffff;  height: 90px;  padding-top: 30px">
                <b style="font-size: 50px;"> 
                    Create new rental car
                </b>
                </div>      
            
            <div style="padding-left: 50px; padding-bottom: 30px; padding-top: 20px">
                <div style="padding-top: 20px; padding-bottom: 10px">
                    <b style="font-weight: bold; font-size: 20px">
                        Vehicle name
                    </b>
                    <select style="margin-left: 13px" name='vehicleName'>
                        <c:forEach var="vehicle" items="${requestScope.LIST_OF_VEHICLE}">
                                <option value="${vehicle.vehicleName}" > ${vehicle.vehicleName}</option>
                        </c:forEach>                            
                    </select>
                </div>
                <div style="padding-top: 20px; padding-bottom: 30px">
                    <b style="font-weight: bold; font-size: 20px">
                         Car type
                    </b>
                    <select  style="margin-left: 13px" name='typeCar'>
                        <c:forEach var="vehicleType" items="${applicationScope.LIST_TYPE_CAR}">
                                <option value="${vehicleType.carTypeName}" > ${vehicleType.carTypeName}</option>
                        </c:forEach>                            
                    </select>
                </div>
                <div>
                    <b style="font-weight: bold; font-size: 20px">
                        Price
                    </b><br/>
                    <input type="text" id="txtPrice" name="txtPrice" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px;" />
                    <h5>${requestScope.Error.priceErrorMsg}</h5>
                </div>  
            </div>
            <div style="text-align: center; padding-bottom: 20px; border: none">
                <button type="submit" value="Create Rental Car" name="btnAction"
                        style="background-color: #04AA6D; color: #ffffff; padding: 14px 20px; border: none">
                    <b style="font-weight: bold">
                        Create Rental Car
                    </b>
                </button>
                <h5>${requestScope.Success}</h5>
            </div>
        </form>
    </body>
</html>
