<%-- 
    Document   : home
    Created on : Oct 25, 2022, 9:30:55 PM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div>

        <div style="margin-left: 50px; margin-bottom: 10px; size: 30px">
            <form action="ServletDispatcher">
                <b>Search user name </b> <input style="background-color: #fff "
                                                type="text" name="txtsearch" value ="${param.txtsearch}"/>
                <input type="submit" name="btnAction" value="Search"/>
            </form>
        </div>
        ${requestScope.UpdateID}   
        ${requestScope.Error.dobErrorMsg}

        <c:if test="${requestScope.LIST_USER != null}">
            <c:if test="${not empty requestScope.LIST_USER}">
                <table border="2" style="background-color: #fff; padding-bottom: 10px; margin-left: 20px">
                    <thead>
                        <tr>
                            <th style="padding-left: 10px">
                                NO
                            </th>
                            <th style="padding-left: 30px">
                                User ID
                            </th>
                            <th>Full Name</th>
                            <th>DOB</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Wallet</th>
                            <th>Password</th>
                            <th>Role</th>
                            <th style="padding-right: 30px">
                                Delete
                            </th>
                            <th style="padding-right: 50px">
                                Update
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="user" varStatus="counter" items="${requestScope.LIST_USER}">
                        <form action="ServletDispatcher" method="POST">
                            <tr>
                                <td style="padding-bottom: 60px; padding-left: 10px; ">
                                    ${counter.count}
                                </td>
                                <td style="padding-bottom: 60px; padding-right: 50px">
                                    <input type="text" name="userID" value="${user.id}" readonly=""
                                           style="margin-right: -180px; margin-left: 30px; padding-right: 20px "
                                           />                                  
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="text" name="fullName" value="${user.fullName}" readonly=""/>
                                    <c:if test="${user.id eq requestScope.UpdateID}">
                                        <h1>${requestScope.Error.fullnameErrorMsg}</h1>
                                    </c:if>                             
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="text" name="dob" value="${user.dob}" readonly="" />
                                    <c:if test="${user.id eq requestScope.UpdateID}">
                                        <h6>${requestScope.Error.dobErrorMsg}</h6>
                                    </c:if>                      
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="text" name="address" value="${user.address}" required=""/>
                                </td>   
                                <td style="padding-bottom: 60px">
                                    <input type="text" name="phone" value="${user.phone}" readonly=""/>
                                    <c:if test="${user.id eq requestScope.UpdateID}">
                                        <h1>${requestScope.Error.phoneErrorMsg}</h1>
                                    </c:if>
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="text" name="email" value="${user.email}" readonly=""/>
                                    <c:if test="${user.id eq requestScope.UpdateID}">
                                        <h1>${requestScope.Error.emailErrorMsg}</h1>
                                    </c:if>
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="text" name="deposit" value="${user.depositDTO.deposit} VNĐ" readonly="" size="20px"/>
                                </td>
                                <td style="padding-bottom: 60px; padding-right: 10px">
                                    <input type="text" name="password" value="${user.password}" readonly="" size="5px"/>
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="text" name="roleName" value="${user.roleDTO.roleName}" required="" size="4px"
                                           style="margin-right: 30px"/>
                                    <c:if test="${user.id eq requestScope.UpdateID}">
                                        <h6>${requestScope.Error.roleErrorMsg}</h6>
                                    </c:if>
                                </td>
                                <td style="padding-bottom: 60px">
                                    <c:url var="deleteLink" value="ServletDispatcher">
                                        <c:param name="btnAction" value="Delete"></c:param>
                                        <c:param name="userID" value="${user.id}"></c:param>
                                        <c:param name="search" value="${param.txtsearch}"></c:param>
                                    </c:url>
                                    <a href="${deleteLink}">Delete</a>
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="submit" name="btnAction" value="Update"/>
                                    <input type="hidden" name="search" value="${param.txtsearch}"/>
                                </td>
                            </tr>
                        </form>
                    </c:forEach>             
                </tbody>
            </table>
        </c:if>
    </c:if>
</body>
</html>
