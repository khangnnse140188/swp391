<%-- 
    Document   : viewRentalCar
    Created on : Nov 17, 2022, 6:41:27 PM
    Author     : KenLy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View ticket page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div> 

    <c:if test="${requestScope.LIST_RENTAL_CAR != null}">
        <c:if test="${not empty requestScope.LIST_RENTAL_CAR}">
            <div style="margin-left: 50px; margin-bottom: 10px; size: 30px">
                <form  action="ServletDispatcher" method="POST">
                    <b>Search Rental Car</b> <input style="background-color: #fff "
                                                type="text" name="txtsearchRentalCar" value ="${param.txtsearchRentalCar}"/>
                    <input type="submit" name="btnAction" value="Search Rental Car"/>
                    <input type="submit" value="Add Rental Car" name="btnAction"
                           style="margin-left: 20px"/>
                </form>
            </div>
            <br/>
            <table border="2" style="background-color: #fff; margin-left: 50px">

                <thead>
                    <tr>
                        <th style="padding-left: 10px">
                            NO
                        </th>
                        <th style="padding-left: 30px">
                            Full Name
                        </th>
                        <th style="padding-left: 30px">
                            Vehicle Name
                        </th>
                        <th style="padding-left: 10px">
                            License flate
                        </th>
                        <th style="padding-left: 10px">
                            Price
                        </th>
                        <th style="padding-right: 30px; padding-left: 20px">
                            Delete
                        </th>
                        <th style="padding-right: 30px; padding-left: 9px">
                            Update
                        </th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="rental" varStatus="counter" items="${requestScope.LIST_RENTAL_CAR}">
                    <form action="ServletDispatcher" method="POST">
                        <tr>
                            <td style="padding-bottom: 60px; padding-left: 10px;">
                                ${counter.count}
                            </td>
                        <input type="hidden" name="rentID" value="${rental.rentCarID}" />

                            <td style="padding-bottom: 60px; padding-right: 50px">
                                <input type="text" name="fullName" value="${rental.userDTO.fullName}" readonly="" size="5px"
                                       style="margin-right: -180px; margin-left: 30px"/>                                  
                            </td>
                            <td style="padding-bottom: 60px; padding-left: 30px">
                                
                               <select  style="margin-left: 13px" name='vehicleName'>
                                   <c:forEach var="vehicle" items="${requestScope.ListVehicle}">
                            <c:if test="${rental.vehicleDTO.vehicleName eq vehicle.vehicleName}">
                                <option value="${vehicle.vehicleName}" selected="" > ${vehicle.vehicleName}</option>
                            </c:if>
                            <c:if test="${rental.vehicleDTO.vehicleName ne vehicle.vehicleName}">
                                <option value="${vehicle.vehicleName}"  > ${vehicle.vehicleName}</option>
                            </c:if>
                        </c:forEach>                            
                    </select>              
                                                           
                            </td>
                            <td style="padding-bottom: 60px; padding-left: 10px">
                                <input type="text" name="license" value="${rental.vehicleDTO.licenseFlate}" required=""/>
                                <c:if test="${rental.rentCarID eq requestScope.UpdateID}">
                                    <h5>${requestScope.Error.licenseFlateErrorMsg}</h5>
                                </c:if>
                            </td>   
                            <td style="padding-bottom: 60px; padding-left: 10px">
                                <input type="text" name="price" value="${rental.priceDTO.price}" required=""/>
                                <c:if test="${rental.rentCarID eq requestScope.UpdateID}">
                                    <h5>${requestScope.Error.priceErrorMsg}</h5>
                                </c:if>
                            </td>

                            <td style="padding-bottom: 60px; padding-left: 20px">
                        <c:url var="deleteLink" value="ServletDispatcher">
                            <c:param name="btnAction" value="Delete Rental Car"></c:param>
                            <c:param name="rentalID" value="${rental.rentCarID}"></c:param>
                            <c:param name="search" value="${param.txtsearchRentalCar}"></c:param>
                        </c:url>
                        <a href="${deleteLink}">Delete</a>
                        </td>
                        <td style="padding-bottom: 60px">
                            <input type="submit" name="btnAction" value="Update RentCar"/>
                            <input type="hidden" name="search" value="${param.txtsearchRentalCar}" />
                        </td>
                        </tr>
                    </form>
                </c:forEach>             
                </tbody>
            </table>
        </c:if>
    </c:if>

</body>
</html>
