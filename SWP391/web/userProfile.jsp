<%-- 
    Document   : userProfile
    Created on : Nov 17, 2022, 10:58:02 PM
    Author     : KenLy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Profile</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div> 
            <form action="ServletDispatcher" method="POST"
               style="background-color: #ffffff; display: inline-block; 
               border: #000; width: 30%; margin-left: 35%">
                <div>
                <div style="text-align: center; background-color: #8223E7; color: #ffffff;  height: 90px;  padding-top: 30px">
                    <b style="font-size: 50px;"> 
                        Change information
                    </b>
                </div>      
            
                <div style="padding-left: 50px; padding-bottom: 20px; padding-top: 20px">
                    <div>
                        <b style="font-weight: bold; font-size: 20px">
                            User name
                        <b></br>
                        <input type="text" id="txtuserName" name="txtUserName" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px;" />
                    </div>  
                </div>
                
                <div style="padding-left: 50px; padding-bottom: 20px; padding-top: 20px">
                    <div>
                        <b style="font-weight: bold; font-size: 20px">
                            DOB
                        <b></br>
                        <input type="text" id="txtdob" name="txtdob" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px;" />
                    </div>  
                </div>
                
                <div style="padding-left: 50px; padding-bottom: 20px; padding-top: 20px">
                    <div>
                        <b style="font-weight: bold; font-size: 20px">
                            Address
                        <b></br>
                        <input type="text" id="txtAddress" name="txtAddress" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px;" />
                    </div>  
                </div>
                
                <div style="padding-left: 50px; padding-bottom: 10px; padding-top: 20px">
                    <div>
                        <b style="font-weight: bold; font-size: 20px">
                            Phone
                        <b></br>
                        <input type="text" id="txtPhone" name="txtPhone" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px;" />
                    </div>  
                </div>
                
                <div style="padding-left: 50px; padding-bottom: 10px; padding-top: 20px">
                    <div>
                        <b style="font-weight: bold; font-size: 20px">
                            Email
                        <b></br>
                        <input type="text" id="txtEmail" name="txtEmail" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px;" />
                    </div>  
                </div>
                
                <div style="padding-left: 50px; padding-bottom: 10px; padding-top: 20px">
                    <div>
                        <b style="font-weight: bold; font-size: 20px">
                            Wallet
                        <b></br>
                        <input type="text" id="txtWallet" name="txtWallet" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px;" />
                    </div>  
                </div>
            </div>
            <div style="text-align: center; padding-bottom: 20px; border: none"> 
                <button type="submit" value="Cancel" name="btnAction"
                        style="background-color: #04AA6D; color: #ffffff; padding: 14px 20px; border: none">
                    <b style="font-weight: bold">
                        Cancel
                    </b>
               </button>
                         
                <button type="submit" value="ChangeProfile" name="btnAction"
                        style="background-color: #04AA6D; color: #ffffff; padding: 14px 20px; border: none">
                    <b style="font-weight: bold">
                        Change
                    </b>
                </button>
                
                <button type="submit" value="Deposit" name="btnAction"
                        style="background-color: #04AA6D; color: #ffffff; padding: 14px 20px; border: none">
                    <b style="font-weight: bold">
                        Deposit
                    </b>
                </button>
            </div>
                
                
        </form>
</body>
</html>
