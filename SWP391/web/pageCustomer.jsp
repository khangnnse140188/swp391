<%-- 
    Document   : home
    Created on : Oct 25, 2022, 9:30:55 PM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Customer Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div> 
        <c:if test="${requestScope.LIST_OF_TRIP != null}">
            <c:if test="${not empty requestScope.LIST_OF_TRIP}">
                <div style="margin-left: 50px; margin-bottom: 10px; size: 30px">
                    <form  action="ServletDispatcher" method="POST">
                        <b>Search trip</b> <input style="background-color: #fff "
                                                  type="text" name="txtsearchTrip" value ="${param.txtsearchTrip}"/>
                        <input type="submit" name="btnAction" value="Search Trip"/>
                        <input type="submit" value="Add Trip" name="btnAction" 
                               style="margin-left: 20px"/>
                        <input type="submit" value="View Customer Ticket" name="btnAction" style="margin-left: 50px"/>

                    </form>
                    <br/>
                </div>
                <br/>
                <table border="2" style="background-color: #fff; margin-left: 50px">
                    <thead>
                        <tr>
                            <th style="padding-left: 10px">
                                NO
                            </th>
                            <th style="padding-left: 30px">
                                Trip ID
                            </th>
                            <th style="padding-left: 30px">
                                Trip name
                            </th>
                            <th style="padding-left: 10px">
                                Description
                            </th>
                            <th style="padding-left: 10px">
                                Start destination
                            </th>
                            <th style="padding-left: 10px">
                                End destination
                            </th >
                            <th style="padding-left: 10px">
                                Start time
                            </th>
                            <th style="padding-left: 10px">
                                Arrive time
                            </th>
                            <th style="padding-right: 30px; padding-left: 20px">
                                Buy a ticket
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="trip" varStatus="counter" items="${requestScope.LIST_OF_TRIP}">
                        <form action="ServletDispatcher" method="POST">
                            <tr>
                                <td style="padding-bottom: 60px; padding-left: 10px;">
                                    ${counter.count}
                                </td>
                                <td style="padding-bottom: 60px; padding-right: 50px">
                                    <input type="text" name="tripID" value="${trip.tripID}" readonly="" size="5px"
                                           style="margin-right: -180px; margin-left: 30px"
                                           />                                  
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 30px">
                                    <input type="text" name="tripName" value="${trip.tripName}" required=""/>                           
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="description" value="${trip.description}" required="" />                     
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="startDes" value="${trip.destinationDTO.startDes}" required=""/>
                                </td>   
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="endDes" value="${trip.destinationDTO.endDes}" required=""/>

                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="startTime" value="${trip.destinationDTO.startTime}" required=""/>
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="ArriveTime" value="${trip.destinationDTO.arriveTime} " readonly="" size="20px"/>
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 20px">
                                    <c:url var="deleteLink" value="ServletDispatcher">
                                        <c:param name="btnAction" value="DeleteTrip"></c:param>
                                        <c:param name="tripID" value="${trip.tripID}"></c:param>
                                    </c:url>
                                    <a href="${deleteLink}">Delete</a>
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="submit" name="btnAction" value="Update Trip"/>
                                </td>
                            </tr>
                        </form>
                    </c:forEach>             
                </tbody>
            </table>
        </c:if>
    </c:if>


    <c:if test="${requestScope.VehicleList != null}">
        <c:if test="${not empty requestScope.VehicleList}">
            <div style="margin-left: 50px; margin-bottom: 10px; size: 30px">
                <form action="ServletDispatcher" method="POST">
                    <b>Search vehicle</b> <input style="background-color: #fff "
                                                 type="text" name="txtsearchVehicle" value ="${param.txtsearchVehicle}"/>
                    <input type="submit" name="btnAction" value="Search Vehicle"/>
                    <input type="submit" value="View Rental Car" name="btnAction" style="margin-left: 50px"/>

                </form>
                <br/>
                <form action="addVehicle.jsp">
                    <input type="submit" value="Add Vehicle" name="btnAction" 
                           style="margin-left: 20px"/>
                </form>
            </div>
            ${requestScope.UpdateID} 
            <br/>
            <table border="2" style="background-color: #fff; padding-bottom: 10px; margin-left: 50px">
                <thead>
                    <tr>
                        <th style="padding-left: 10px">
                            NO
                        </th>
                        <th style="padding-left: 40px">
                            Vehicle ID
                        </th>
                        <th style="padding-left: 40px">
                            Vehicle name
                        </th>
                        <th style="padding-left: 25px">
                            License Flate
                        </th>
                        <th style="padding-left: 30px">
                            Car Type
                        </th>
                        <th style="padding-left: 10px">
                            Number Of Seat
                        </th>
                        <th style="padding-right: 30px; padding-left: 30px">
                            Rent
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="vehicle" varStatus="counter" items="${requestScope.VehicleList}">
                    <form action="ServletDispatcher" method="POST">
                        <tr>
                            <td style="padding-bottom: 60px; padding-left: 10px; ">
                                ${counter.count}
                            </td>
                            <td style="padding-bottom: 60px; padding-right: 50px">
                                <input type="text" name="vehicleID" value="${vehicle.vehicleID}" readonly="" size="8px"
                                       style="margin-right: -180px; margin-left: 30px; "
                                       />                                  
                            </td>
                            <td style="padding-bottom: 60px; padding-left: 30px">
                                <input type="text" name="vehicleName" value="${vehicle.vehicleName}" required=""/>                           
                            </td>
                            <td style="padding-bottom: 60px; padding-left: 20px">
                                <input type="text" name="licenseFlate" value="${vehicle.licenseFlate}" required="" size="10px"/>                     
                            </td>
                            <td style="padding-bottom: 60px; padding-left: 10px">
                                <select>
                                    <c:forEach var="vehicleType" items="${applicationScope.LIST_TYPE_CAR}">
                                        <c:if test="${vehicleType.carTypeName eq vehicle.carTypeDTO.carTypeName}">
                                            <option value="${vehicleType.carTypeName}" selected > ${vehicleType.carTypeName}</option>
                                        </c:if>
                                        <c:if test="${vehicleType.carTypeName != vehicle.carTypeDTO.carTypeName}">
                                            <option value="${vehicleType.carTypeName}" > ${vehicleType.carTypeName}</option>
                                        </c:if>

                                    </c:forEach>                            
                                </select>
                            </td>
                            <td style="padding-bottom: 60px">
                                <input type="text" name="numberOfSeat" value="${vehicle.carTypeDTO.numberOfSeat}" required=""
                                       size="5px" style="margin-left: 30px"/>
                            </td>
                            <td style="padding-bottom: 60px; padding-left: 30px">
                                <c:url var="deleteLink" value="ServletDispatcher">
                                    <c:param name="btnAction" value="DeleteVehicle"></c:param>
                                    <c:param name="userID" value="${vehicle.vehicleID}"></c:param>
                                </c:url>
                                <a href="${deleteLink}">Delete</a>
                            </td>
                            <td style="padding-bottom: 60px">
                                <input type="submit" name="btnAction" value="Update"/>
                            </td>
                        </tr>
                    </form>
                </c:forEach>             
            </tbody>
        </table>
    </c:if>
</c:if>  
</body>
</html>
