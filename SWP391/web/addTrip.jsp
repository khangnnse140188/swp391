<%-- 
    Document   : createTrip
    Created on : Nov 16, 2022, 7:41:21 PM
    Author     : KenLy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add trip page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
        <link href="css/Addtrip.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div> 
        
        <form action="ServletDispatcher" method="POST"
               style="background-color: #ffffff; display: inline-block; 

               border: #000; width: 30%; margin-left: 35%">
            
            <div style="text-align: center; background-color: #8223E7; color: #ffffff; height: 90px;  padding-top: 30px">
                
                <b style="font-size: 50px;"> 
                    Create new Trip 
                </b>
            </div>
            
            <div style="padding-left: 50px; padding-bottom: 30px; padding-top: 20px">
                <div>
                    <b style="font-weight: bold; font-size: 20px">
                        Trip name
                    </b></br>
                    <input type="text" id="txtTripName" name="txtTripName" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none; 
                           margin-bottom: 10px; margin-top: 10px"/>
                    ${requestScope.Trip_Error.tripNameError}
                </div>
                
                <div>
                    <b style="font-weight: bold; font-size: 20px">
                        Description
                    </b><br/>
                    <input type="text" id="txtDescription" name="txtDescription" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px; margin-top: 10px"/>
                </div>
                
                <div>
                    <b style="font-weight: bold; font-size: 20px">
                        Start Des
                    <b></br>
                    <input type="text" id="txtStartDes" name="txtStartDes" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px; margin-top: 10px"/>
                    ${requestScope.Price_Error.startDesError}
                </div>
                
                <div>
                    <b style="font-weight: bold; font-size: 20px">
                        End Des
                    </b>
                    <br/>
                    <input type="text" id="txtEndDes" name="txtEndDes" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none;
                           margin-bottom: 10px; margin-top: 10px"/>
                    ${requestScope.Price_Error.endDesError}
                </div>
                
                <div style="padding-top: 20px">
                    <b style="font-weight: bold; font-size: 20px">
                        Start time
                    </b>
                      <input type="datetime-local" step="2" id="txtStartTime" name="txtStartTime" required="required" />
                </div>
                
                <div style="padding-top: 20px">
                    <b style="font-weight: bold; font-size: 20px">
                        End time
                    </b> 
                     <input type="datetime-local" step="2"  id="txtArriveTime" name="txtArriveTime" required="required"
                           style="margin-left: 10px"/>

                     ${requestScope.Price_Error.endTimeError}
                </div>
                
                <div>
                    <b style="font-weight: bold; font-size: 20px">
                        Price
                    </b></br>
                    <input type="text" id="txtPrice" name="txtPrice" required="required" size="48%"
                           style="background-color: #EEEEEE; border: none; 
                           margin-bottom: 10px; margin-top: 10px"/>
                </div>   
                <div style="">
                    <b style="font-weight: bold; font-size: 20px">
                        Choose Vehicle
                    </b>
                    <select name="vehicle">
                        <c:forEach var="vehicle" items="${applicationScope.Vehicle_List_For_Trip}">
                                <option value="${vehicle.vehicleName}"  > ${vehicle.vehicleName}</option>
                        </c:forEach>                            
                    </select>                
                </div>  
            </div>
                     
            </div>
            
            <div style="text-align: center; padding-bottom: 20px; border: none">
                <button type="submit" value="Create Trip" name="btnAction"
                        style="background-color: #04AA6D; color: #ffffff; padding: 14px 20px; border: none">
                    <b style="font-weight: bold">
                        Create Trip
                    </b>
                </button>
            </div>           
        </form>
            <div style="text-align: center; padding-bottom: 20px; color: red;">
                <b style="font-weight: bold">
                    ${requestScope.Success}
                </b>      
            </div>
    </body>
</html>
