<%-- 
    Document   : viewTicket
    Created on : Nov 17, 2022, 6:41:09 PM
    Author     : KenLy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View ticket page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    </head>
    <body>
        <%@include file="navbar.jsp"%>
        <div style="height:125px; width:100%; clear:both;"></div> 

        <c:if test="${requestScope.LIST_OF_TICKET != null}">
            <c:if test="${not empty requestScope.LIST_OF_TICKET}">
                <div style="margin-left: 50px; margin-bottom: 10px; size: 30px">
                    <form  action="ServletDispatcher" method="POST">
                        <b>Search ticket</b> <input style="background-color: #fff "
                                                    type="text" name="txtsearchTicket" value ="${param.txtsearchTicket}"/>
                        <input type="submit" name="btnAction" value="SearchTicket"/>
                        <input type="submit" value="Add Ticket" name="btnAction"
                               style="margin-left: 20px"/>
                    </form>
                </div>
                <br/>
                <table border="2" style="background-color: #fff; margin-left: 50px">
                    <thead>
                        <tr>
                            <th style="padding-left: 10px">
                                NO
                            </th>
                            <th style="padding-left: 30px">
                                Trip name
                            </th>
                            <th style="padding-left: 30px">
                                Price
                            </th>
                            <th style="padding-left: 30px">
                                Start destination
                            </th>
                            <th style="padding-left: 10px">
                                End destination
                            </th>
                            <th style="padding-left: 10px">
                                Start time
                            </th>
                            <th style="padding-left: 10px">
                                Arrive time
                            </th >
                            <th style="padding-left: 10px">
                                Seat position
                            </th>
                            <th style="padding-left: 10px">
                                Full name
                            </th>
                            <th style="padding-right: 30px; padding-left: 20px">
                                Delete
                            </th>
                            <th style="padding-right: 30px; padding-left: 9px">
                                Update
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="ticket" varStatus="counter" items="${requestScope.LIST_OF_TICKET}">
                        <form action="ServletDispatcher" method="POST">
                            <tr>
                                <td style="padding-bottom: 60px; padding-left: 10px;">
                                    ${ticket.count}
                                </td>
                                <td style="padding-bottom: 60px; padding-right: 50px">
                                    <input type="text" name="ticketID" value="${ticket.tripID}" readonly="" size="5px"
                                           style="margin-right: -180px; margin-left: 30px"/>                                  
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 30px">
                                    <input type="text" name="ticketName" value="${ticket.tripName}" required=""/>                           
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="price" value="${ticket.description}" required="" />                     
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="startDes" value="${ticket.destinationDTO.startDes}" required=""/>
                                </td>   
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="endDes" value="${ticket.destinationDTO.endDes}" required=""/>

                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="startTime" value="${ticket.destinationDTO.startTime}" required=""/>
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 10px">
                                    <input type="text" name="ArriveTime" value="${ticket.destinationDTO.arriveTime} " readonly="" size="20px"/>
                                </td>
                                <td style="padding-bottom: 60px; padding-left: 20px">
                                    <c:url var="deleteLink" value="ServletDispatcher">
                                        <c:param name="btnAction" value="Delete Trip"></c:param>
                                        <c:param name="ticketID" value="${ticket.tripID}"></c:param>
                                    </c:url>
                                    <a href="${deleteLink}">Delete</a>
                                </td>
                                <td style="padding-bottom: 60px">
                                    <input type="submit" name="btnAction" value="Update RentCar"/>
                                </td>
                            </tr>
                        </form>
                    </c:forEach>             
                </tbody>
            </table>
        </c:if>
    </c:if>

</body>
</html>
