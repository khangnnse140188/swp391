/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CarRentalDTO;
import DTO.DestinationDTO;
import DTO.PriceDTO;
import DTO.TripDTO;
import DTO.VehicleDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class TripDAO {

    public ArrayList<TripDTO> getAllTrip() throws ClassNotFoundException, SQLException {
        ArrayList<TripDTO> tripList = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select tripID, tripName, description, desID,vehicleID,priceID "
                        + "from tblTrip";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();

                tripList = new ArrayList<>();

                while (rs.next()) {
                    int tripID = rs.getInt("tripID");
                    String tripName = rs.getString("tripName");
                    String description = rs.getString("description");
                    int desID = rs.getInt("desID");
                    DestinationDTO destinationDTO = getDestinationByID(desID);
                    int vehicleID = rs.getInt("vehicleID");
                    VehicleDAO vehicleDAO = new VehicleDAO();
                    VehicleDTO vehicleDTO = vehicleDAO.getVehicleByID(vehicleID);
                    PriceDAO priceDAO = new PriceDAO();
                    PriceDTO priceDTO = priceDAO.getPriceByID(rs.getInt("priceID"));
                    if (destinationDTO != null) {
                        TripDTO tripDTO = new TripDTO(tripID, tripName, description, destinationDTO, vehicleDTO, priceDTO);
                        tripList.add(tripDTO);
                    }

                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return tripList;
    }

    public DestinationDTO getDestinationByID(int id) throws ClassNotFoundException, SQLException {
        DestinationDTO destinationDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select desID,startDes,endDes,startTime,arriveTime "
                        + "from tblDestination "
                        + "where desID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    int desID = rs.getInt("desID");
                    String startDes = rs.getString("startDes");
                    String endDes = rs.getString("endDes");
                    Timestamp startTime = rs.getTimestamp("startTime");
                    Timestamp arriveTime = rs.getTimestamp("arriveTime");
                    destinationDTO = new DestinationDTO(desID, startDes, endDes, startTime, arriveTime);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return destinationDTO;
    }

    public ArrayList<TripDTO> searchTrip(String searchValue) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<TripDTO> result = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select tripID,tripName,description,desID,vehicleID,priceID "
                        + "from tblTrip where tripName "
                        + "like ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, "%"+searchValue+"%");
                rs = ps.executeQuery();

                result = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt("tripID");
                    String tripName = rs.getString("tripName");
                    String description = rs.getString("description");
                    int desId = rs.getInt("desID");
                    DestinationDTO destinationDTO = getDestinationByID(id);
                    int vehicleID = rs.getInt("vehicleID");
                    VehicleDAO vehicleDAO = new VehicleDAO();
                    VehicleDTO vehicleDTO = vehicleDAO.getVehicleByID(vehicleID);
                    PriceDAO priceDAO = new PriceDAO();
                    PriceDTO priceDTO = priceDAO.getPriceByID(rs.getInt("priceID"));
                    TripDTO tripDTO = new TripDTO(desId, tripName, description, destinationDTO, vehicleDTO, priceDTO);
                    result.add(tripDTO);
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return result;
    }

    public boolean addTrip(TripDTO tripDTO) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = DBUtils.getConnection();

            if (con != null) {

                String sql = "insert into tblTrip "
                        + "values (?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, tripDTO.getTripName());
                ps.setString(2, tripDTO.getDescription());
                ps.setInt(3, tripDTO.getPriceDTO().getPriceID());
                ps.setInt(4, tripDTO.getDestinationDTO().getDesId());
                ps.setInt(5, tripDTO.getVehicleDTO().getVehicleID());
                if (ps.executeUpdate() > 0) {
                    return true;
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    
    public boolean updateTrip(TripDTO tripDTO) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = DBUtils.getConnection();

            if (con != null) {

                String sql = "update tblTrip "
                        + "set tripName = ?,description = ?,desID = ? , vehicleID = ?  "
                        + "where tripID = ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, tripDTO.getTripName());
                ps.setString(2, tripDTO.getDescription());
                ps.setInt(3, tripDTO.getDestinationDTO().getDesId());
                ps.setInt(4, tripDTO.getVehicleDTO().getVehicleID());
                ps.setInt(5, tripDTO.getTripID());
                if (ps.executeUpdate() > 0) {
                    return true;
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    
    public boolean deleteTrip(int tripID) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = DBUtils.getConnection();

            if (con != null) {

                String sql = "delete from tblTrip "
                        + "where tripID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, tripID);
                if (ps.executeUpdate() > 0) {
                    return true;
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    public int addDestination(DestinationDTO destinationDTO) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = DBUtils.getConnection();

            if (con != null) {

                String sql = "insert into tblDestination "
                        + "values (?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, destinationDTO.getStartDes());
                ps.setString(2, destinationDTO.getEndDes());
                ps.setTimestamp(3, destinationDTO.getStartTime());
                ps.setTimestamp(4, destinationDTO.getArriveTime());
                ps.executeUpdate();
                sql = "Select @@IDENTITY as desID";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getInt("desID");
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return -1;
    }
    
    public int updateDestination(DestinationDTO destinationDTO) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = DBUtils.getConnection();

            if (con != null) {

                String sql = "update tblDestination set startDes = ?, endDes = ?,startTime = ?,arriveTime = ? "
                        + "where desID = ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, destinationDTO.getStartDes());
                ps.setString(2, destinationDTO.getEndDes());
                ps.setTimestamp(3, destinationDTO.getStartTime());
                ps.setTimestamp(4, destinationDTO.getArriveTime());
                ps.setInt(5, destinationDTO.getDesId());
                ps.executeUpdate();
                sql = "select @@IDENTITY as desID";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getInt("desID");
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return -1;
    }
    
    public boolean deleteDestination(int desID) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = DBUtils.getConnection();

            if (con != null) {

                String sql = "delete from tblDestination where desID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, desID);
                if (ps.executeUpdate() > 0) {
                    return true;
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    
    public TripDTO getTripById (int tripID) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        TripDTO tripDTO = null;
        try {
            con = DBUtils.getConnection();

            if (con != null) {

                String sql = "select tripID,tripName,description,priceID,desID,vehicleID from tblTrip where tripID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, tripID);
                rs = ps.executeQuery();
                if (rs.next()){
                    int tripID1 = rs.getInt(1);
                    String tripName = rs.getString(2);
                    String description = rs.getString(3);
                    int priceID = rs.getInt(4);
                    PriceDAO priceDAO = new PriceDAO();
                    PriceDTO priceDTO = priceDAO.getPriceByID(priceID);
                    int desID = rs.getInt(5);
                    DestinationDTO destinationDTO = getDestinationByID(desID);
                    int vehicleID = rs.getInt(6);
                    VehicleDAO vehicleDAO = new VehicleDAO();
                    VehicleDTO vehicleDTO = vehicleDAO.getVehicleByID(vehicleID);
                    tripDTO = new TripDTO(tripID, tripName, description, destinationDTO, vehicleDTO, priceDTO);
                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return tripDTO;
    }
}
