/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CarRentalDTO;
import DTO.CarTypeDTO;
import DTO.DestinationDTO;
import DTO.PriceDTO;
import DTO.TripDTO;
import DTO.UserDTO;
import DTO.VehicleDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class RentCarDAO {

    public ArrayList<CarRentalDTO> getAllRentalCar() throws ClassNotFoundException, SQLException {
        ArrayList<CarRentalDTO> listRentalCar = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        VehicleDAO vehicleDAO = null;
        PriceDAO priceDAO = null;
        UserDAO userDAO = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select rentCarID,vehicleID,priceID,Status,userID "
                        + "from tblCarRental";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();

                DriverDAO driverDAO = new DriverDAO();
                priceDAO = new PriceDAO();
                userDAO = new UserDAO();
                vehicleDAO = new VehicleDAO();
                listRentalCar = new ArrayList<>();

                while (rs.next()) {
                    int rentCarID = rs.getInt("rentCarID");

                    int vehicleID = rs.getInt("vehicleID");

                    VehicleDTO vehicleDTO = vehicleDAO.getVehicleByID(vehicleID);
                    int priceID = rs.getInt("priceID");
                    PriceDTO priceDTO = priceDAO.getPriceByID(priceID);
                    boolean Status = rs.getBoolean("Status");
                    int userID = rs.getInt("userID");
                    UserDTO userDTO = userDAO.getUserByID(userID);
                    
                    
                    
                        CarRentalDTO carRentalDTO = new CarRentalDTO(rentCarID, vehicleDTO, priceDTO,  Status, userDTO);
                        listRentalCar.add(carRentalDTO);


                }

            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return listRentalCar;
    }

//    public ArrayList<CarRentalDTO> searchRentalCar (String searchValue) throws ClassNotFoundException, SQLException{
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs= null;
//        ArrayList<CarRentalDTO> result = null;
//        try {
//            con = DBUtils.getConnection();
//            if (con != null){
//                String sql = "select tripID,tripName,description,desID "
//                        + "from tblTrip where tripName "
//                        + "like ?";
//                ps = con.prepareStatement(sql);
//                ps.setString(1, searchValue);
//                rs = ps.executeQuery();
//                if (rs.next()){
//                   result = new ArrayList<>();
//                   while (rs.next()){
//                       int id = rs.getInt("tripID");
//                       String tripName = rs.getString("tripName");
//                       String description = rs.getString("description");
//                       int desId = rs.getInt("desID");
//                       DestinationDTO destinationDTO = getDestinationByID(id);
//                       TripDTO tripDTO = new TripDTO(desId, tripName, description, destinationDTO);
//                       result.add(tripDTO);
//                   }
//                }
//            }
//        } 
//        finally{
//            if (rs != null){
//                rs.close();
//            }
//            if (ps != null){
//                ps.close();
//            }
//            if (con != null){
//                con.close();
//            }
//        }
//    }
//    
    public ArrayList<CarTypeDTO> getAllCarType() throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CarTypeDTO> result = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select carTypeID,carTypeName,numberOfSeat "
                        + "from tblCarType";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                result = new ArrayList<>();
                while (rs.next()) {
                    int carTypeID = rs.getInt("carTypeID");
                    String carTypeName = rs.getString("carTypeName");
                    String numberOfSeat = rs.getString("numberOfSeat");
                    CarTypeDTO carTypeDTO = new CarTypeDTO(carTypeID, carTypeName, numberOfSeat);
                    result.add(carTypeDTO);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return result;
    }
    
    public ArrayList<CarRentalDTO> getRentalCarByType (String carTypeName) throws ClassNotFoundException, SQLException{
        ArrayList<CarRentalDTO> listRentalCar = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CarTypeDTO> result = null;
        ArrayList<VehicleDTO> vehicleList = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {

                String sql = "select carTypeID,carTypeName,numberOfSeat "
                        + "from tblCarType "
                        + "where carTypeName = ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, carTypeName);
                rs = ps.executeQuery();
                CarTypeDTO carTypeDTO = null;
                if (rs.next()){
                    int carTypeID = rs.getInt("carTypeID");
                    String carTypeName1 = rs.getString("carTypeName");
                    String numberOfSeat = rs.getString("numberOfSeat");
                    carTypeDTO = new CarTypeDTO(carTypeID, carTypeName, numberOfSeat);
                }
                
                 sql = "select vehicleID,vehicleName,licensePlates,carTypeID "
                         + "from tblVehicle "
                         + "where carTypeID = ? ";
                ps = con.prepareStatement(sql);
                
                ps.setInt(1, carTypeDTO.getCarTypeID());
                rs = ps.executeQuery();
                vehicleList = new ArrayList<>();
                while (rs.next()) {
                    int vehicleID = rs.getInt("vehicleID");
                    String vehicleName = rs.getString("vehicleName");
                    String licensePlates = rs.getString("licensePlates");
                    VehicleDTO vehicleDTO = new VehicleDTO(vehicleID, vehicleName, licensePlates, carTypeDTO);
                    vehicleList.add(vehicleDTO);
                }
                DriverDAO driverDAO = new DriverDAO();
                VehicleDAO vehicleDAO = new VehicleDAO();
                PriceDAO priceDAO = new PriceDAO();
                UserDAO userDAO = new UserDAO();
                listRentalCar = new ArrayList<>();
                for (VehicleDTO vehicleDTO : vehicleList) {
                    sql = "select rentCarID,vehicleID,priceID,Status,userID from tblCarRental where  vehicleID = ?";
                    ps = con.prepareStatement(sql);
                    ps.setInt(1, vehicleDTO.getVehicleID());
                    rs = ps.executeQuery();
                    if (rs.next()){
                        int rentCarID = rs.getInt("rentCarID");
                        int vehicleID = rs.getInt("vehicleID");
                        VehicleDTO vehicleDTO1 = vehicleDAO.getVehicleByID(vehicleID);
                        int priceID = rs.getInt("priceID");
                        PriceDTO priceDTO = priceDAO.getPriceByID(priceID);
                        boolean status = rs.getBoolean("Status");
                        int userID = rs.getInt("userID");
                        UserDTO userDTO = userDAO.getUserByID(userID);
                        CarRentalDTO carRentalDTO = new CarRentalDTO(rentCarID, vehicleDTO, priceDTO,  status, userDTO);
                        listRentalCar.add(carRentalDTO);
                    }
                }
                
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return listRentalCar;
    }
    
    public boolean addRentalCar (CarRentalDTO carRentalDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "insert into tblCarRental values (?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, carRentalDTO.getVehicleDTO().getVehicleID());
                ps.setInt(2, carRentalDTO.getPriceDTO().getPriceID());
                ps.setBoolean(3, carRentalDTO.isStatus());
                ps.setNull(4, Types.INTEGER);
                if (ps.executeUpdate() > 0){
                    return true;
                }
            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        
        return false;
    }
    
    public boolean updateRentalCar (CarRentalDTO carRentalDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "update tblCarRental set vehicleID = ?, priceID = ?,  Status = ? , userID = ? where rentCarID = ?";
                ps = con.prepareStatement(sql);
                if (carRentalDTO == null){
                    System.out.println("car rental null");
                }
                ps.setInt(1, carRentalDTO.getVehicleDTO().getVehicleID());
                ps.setInt(2, carRentalDTO.getPriceDTO().getPriceID());
                ps.setBoolean(3, false);
                ps.setInt(4, carRentalDTO.getUserDTO().getId()); 
                ps.setInt(5, carRentalDTO.getRentCarID());
                if (ps.executeUpdate() > 0){
                    return true;
                }
            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        
        return false;
    }
    
    public boolean deleteRentalCar (int rentCarID) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "delete from tblCarRental where rentCarID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, rentCarID);
                if (ps.executeUpdate() > 0){
                    return true;
                }
            }
        } finally {

            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        
        return false;
    }
    
    public ArrayList<CarRentalDTO> searchRentalCar(String search) throws ClassNotFoundException, SQLException  {
        ArrayList<CarRentalDTO> result = null;
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DBUtils.getConnection();
            if (conn != null) {
                
                VehicleDAO vehicleDAO = new VehicleDAO();
                PriceDAO priceDAO = new PriceDAO();
                UserDAO userDAO = new UserDAO();
                ArrayList<VehicleDTO> listVehicleDTO = vehicleDAO.searchVehicle(search);
                result = new ArrayList<>();
                for (VehicleDTO vehicleDTO : listVehicleDTO) {
                    String sql = "select rentCarID,vehicleID,priceID,Status,userID from tblCarRental where vehicleID = ?";
                    stm = conn.prepareStatement(sql);
                    stm.setInt(1, vehicleDTO.getVehicleID());
                    rs = stm.executeQuery();
                    while (rs.next()){
                        int rentCarID = rs.getInt(1);
                        int vehicleID = rs.getInt(2);
                        
                        int priceID = rs.getInt(3);
                        boolean status = rs.getBoolean(4);
                        int userID = rs.getInt(5);
                        CarRentalDTO carRentalDTO = new CarRentalDTO(rentCarID, vehicleDAO.getVehicleByID(vehicleID), priceDAO.getPriceByID(priceID), status,userDAO.getUserByID(userID) );
                        result.add(carRentalDTO);
                    }
                }
                
            }
        } 
        finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }
    
    public CarRentalDTO getRentCarByID (int rentID) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        CarRentalDTO carRentalDTO = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select rentCarID,vehicleID,priceID,Status,userID from tblCarRental where rentCarID = ? ";
                ps = con.prepareStatement(sql);
                ps.setInt(1, rentID);
                rs = ps.executeQuery();
                if (rs.next()){
                    int rentCarID = rs.getInt(1);
                    int vehicleID = rs.getInt(2);
                    VehicleDAO vehicleDAO = new VehicleDAO();
                    PriceDAO priceDAO = new PriceDAO();
                    UserDAO userDAO = new UserDAO();
                    int priceID = rs.getInt(3);
                    boolean status = rs.getBoolean(4);
                    int userID = rs.getInt(5);
                    carRentalDTO = new CarRentalDTO(rentCarID, vehicleDAO.getVehicleByID(vehicleID), priceDAO.getPriceByID(priceID), status, userDAO.getUserByID(userID));
                }
            }
        } finally {
            if (rs != null){
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        }
        
        return carRentalDTO;
    }
}
