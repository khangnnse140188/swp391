/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.PriceDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class PriceDAO {
    public PriceDTO getPriceByID (int id) throws ClassNotFoundException, SQLException{
        PriceDTO priceDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select priceID,price  "
                        + "from tblPrice "
                        + "where priceID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int priceID = rs.getInt("priceID");
                    float price = rs.getFloat("price");
                        priceDTO = new PriceDTO(priceID, price);
                }
            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return priceDTO;
    }
    
//    public TypeOfPriceDTO getTypeOfPriceByID (int id) throws ClassNotFoundException, SQLException{
//        TypeOfPriceDTO typeOfPriceDTO = null;
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            con = DBUtils.getConnection();
//            if (con != null) {
//                String sql = "select TypeID,TypeName "
//                        + "from tblTypeOfPrice "
//                        + "where TypeID = ?";
//                ps = con.prepareStatement(sql);
//                ps.setInt(1, id);
//                rs = ps.executeQuery();
//                if (rs.next()){
//                    int TypeID = rs.getInt("TypeID");
//                    String TypeName = rs.getString("TypeName");
//                    typeOfPriceDTO = new TypeOfPriceDTO(TypeID, TypeName);
//                }
//
//            }
//        } 
//        finally{
//            if (rs != null) {
//                rs.close();
//            }
//            if (ps != null) {
//                ps.close();
//            }
//            if (con != null){
//                con.close();
//            }
//        }
//        return  typeOfPriceDTO;
//    }
    
    public int AddPrice (PriceDTO priceDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "insert into tblPrice values (?)";
                ps = con.prepareStatement(sql);
                ps.setFloat(1, priceDTO.getPrice());
                ps.executeUpdate();
                sql = "select @@IDENTITY as priceID";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getInt("priceID");
                }

            }
        } 
        finally{
            if (rs != null){
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return -1;
    }
    
    public boolean UpdatePrice (PriceDTO priceDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "update tblPrice set price = ? where priceID = ?";
                ps = con.prepareStatement(sql);
                ps.setFloat(1, priceDTO.getPrice());
                ps.setInt(2, priceDTO.getPriceID());
                if (ps.executeUpdate() > 0){
                    return true;
                }
            }
        } 
        finally{
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return false;
    }
    public boolean DeletePrice (int priceID) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "delete from tblPrice where priceID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, priceID);
                if (ps.executeUpdate() > 0){
                    return true;
                }
            }
        } 
        finally{
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return false;
    }
    
}
