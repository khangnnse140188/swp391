/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CarTypeDTO;
import DTO.VehicleDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import utils.DBUtils;

/**
 *
 * @author khang
 */
public class VehicleDAO {
    public VehicleDTO getVehicleByID (int id) throws ClassNotFoundException, SQLException{
        VehicleDTO vehicleDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select vehicleID,vehicleName,licensePlates,carTypeID "
                        + "from tblVehicle "
                        + "where vehicleID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int vehicleID = rs.getInt("vehicleID");
                    String vehicleName = rs.getString("vehicleName");
                    String licensePlates = rs.getString("licensePlates");
                    int carTypeID = rs.getInt("carTypeID");
                    CarTypeDTO carTypeDTO = getCarTypeByID(carTypeID);
                    
                        vehicleDTO = new VehicleDTO(vehicleID, vehicleName, licensePlates, carTypeDTO);
                    
                    
                }

            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return vehicleDTO;
    }
    
    public CarTypeDTO getCarTypeByID (int id) throws ClassNotFoundException, SQLException{
        CarTypeDTO carTypeDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select carTypeID,carTypeName,numberOfSeat "
                        + "from tblCarType "
                        + "where carTypeID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()){
                    int carTypeID = rs.getInt("carTypeID");
                    String carTypeName = rs.getString("carTypeName");
                    String numberOfSeat = rs.getString("numberOfSeat");
                    carTypeDTO = new CarTypeDTO(carTypeID, carTypeName, numberOfSeat);
                }
            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return carTypeDTO;
    }
    
        
        
    
    
    public ArrayList<VehicleDTO> getAllVehicle () throws ClassNotFoundException, SQLException{
        ArrayList<VehicleDTO> result = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select vehicleID,vehicleName,licensePlates,carTypeID "
                        + "from tblVehicle";
                ps = con.prepareStatement(sql);

                rs = ps.executeQuery();
                result = new ArrayList<>();
                while (rs.next()){
                    int vehicleID = rs.getInt("vehicleID");
                    String vehicleName = rs.getString("vehicleName");
                    String licenseFlate = rs.getString("licensePlates");
                    int carTypeID = rs.getInt("carTypeID");
                    CarTypeDTO carTypeDTO = getCarTypeByID(carTypeID);
                    VehicleDTO vehicleDTO = new VehicleDTO(vehicleID, vehicleName, licenseFlate, carTypeDTO);
                    result.add(vehicleDTO);
                }

            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return result;
    }
    
    public boolean addVehicle (VehicleDTO vehicleDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;

            try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "insert into tblVehicle values (?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1, vehicleDTO.getVehicleName());
                ps.setString(2, vehicleDTO.getLicenseFlate());
                ps.setInt(3, vehicleDTO.getCarTypeDTO().getCarTypeID());
  
                if (ps.executeUpdate() > 0){
                    return true;
                }

            }
        } 
        finally{
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return false;
    }
    
    public boolean updateVehicle (VehicleDTO vehicleDTO) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;

            try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "update tblVehicle set vehicleName = ?, licensePlates = ? , carTypeID = ? "
                        + "where vehicleID = ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, vehicleDTO.getVehicleName());
                ps.setString(2, vehicleDTO.getLicenseFlate());
                ps.setInt(3, vehicleDTO.getCarTypeDTO().getCarTypeID());
                ps.setInt(4, vehicleDTO.getVehicleID());
  
                if (ps.executeUpdate() > 0){
                    return true;
                }

            }
        } 
        finally{
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return false;
    }
    
    public boolean DeleteVehicle (int vehicleID) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement ps = null;

            try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "delete from tblVehicle "
                        + "where vehicleID = ?";
                ps = con.prepareStatement(sql);
                ps.setInt(1, vehicleID);
  
                if (ps.executeUpdate() > 0){
                    return true;
                }

            }
        } 
        finally{
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return false;
    }
    
     public ArrayList<VehicleDTO> searchVehicle(String search) throws ClassNotFoundException, SQLException  {
        ArrayList<VehicleDTO> result = new ArrayList<>();
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DBUtils.getConnection();
            if (conn != null) {
                String sql = "select vehicleID,vehicleName,licensePlates,carTypeID "
                        + "from tblVehicle "
                        + "where vehicleName like ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + search + "%");
                rs = stm.executeQuery();
                result = new ArrayList<>();
                while (rs.next()) {
                    int vehicleID = rs.getInt("vehicleID");
                    String vehicleName = rs.getString("vehicleName");
                    String licenseFlates = rs.getString("licensePlates");
                    int carTypeID = rs.getInt("carTypeID");
                    CarTypeDTO carTypeDTO = getCarTypeByID(carTypeID);
                    VehicleDTO vehicleDTO= new VehicleDTO(vehicleID, vehicleName, licenseFlates, carTypeDTO);
                    result.add(vehicleDTO);
                }
            }
        } 
        finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }
     
     public VehicleDTO GetVehicleByName(String vehicleName) throws ClassNotFoundException, SQLException  {
        VehicleDTO vehicleDTO = null ;
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DBUtils.getConnection();
            if (conn != null) {
                String sql = "select vehicleID,vehicleName,licensePlates,carTypeID "
                        + "from tblVehicle "
                        + "where vehicleName = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1,  vehicleName );
                rs = stm.executeQuery();

                if (rs.next()) {
                    int vehicleID = rs.getInt("vehicleID");
                    String vehicleName1 = rs.getString("vehicleName");
                    String licenseFlates = rs.getString("licensePlates");
                    int carTypeID = rs.getInt("carTypeID");
                    CarTypeDTO carTypeDTO = getCarTypeByID(carTypeID);
                    vehicleDTO= new VehicleDTO(vehicleID, vehicleName, licenseFlates, carTypeDTO);

                }
            }
        } 
        finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return vehicleDTO;
    }
     
     public CarTypeDTO getCarTypeByName (String carType) throws ClassNotFoundException, SQLException{
        CarTypeDTO carTypeDTO = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBUtils.getConnection();
            if (con != null) {
                String sql = "select carTypeID,carTypeName,numberOfSeat "
                        + "from tblCarType "
                        + "where carTypeName = ?";
                ps = con.prepareStatement(sql);
                ps.setString(1, carType);
                rs = ps.executeQuery();
                if (rs.next()){
                    int carTypeID = rs.getInt("carTypeID");
                    String carTypeName = rs.getString("carTypeName");
                    String numberOfSeat = rs.getString("numberOfSeat");
                    carTypeDTO = new CarTypeDTO(carTypeID, carTypeName, numberOfSeat);
                }
            }
        } 
        finally{
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null){
                con.close();
            }
        }
        return carTypeDTO;
    }
}
