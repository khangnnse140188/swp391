/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserValidator;

/**
 *
 * @author khang
 */
public class VehicleErrorMessage {
    private String vehicleNameErrorMsg ;
    private String licenseFlateErrorMsg ;
    private String numberSeatErrorMsg;

    public VehicleErrorMessage() {
    }

    public VehicleErrorMessage(String vehicleNameErrorMsg, String licenseFlateErrorMsg, String numberSeatErrorMsg) {
        this.vehicleNameErrorMsg = vehicleNameErrorMsg;
        this.licenseFlateErrorMsg = licenseFlateErrorMsg;
        this.numberSeatErrorMsg = numberSeatErrorMsg;
    }



    public String getVehicleNameErrorMsg() {
        return vehicleNameErrorMsg;
    }

    public void setVehicleNameErrorMsg(String vehicleNameErrorMsg) {
        this.vehicleNameErrorMsg = vehicleNameErrorMsg;
    }

    public String getLicenseFlateErrorMsg() {
        return licenseFlateErrorMsg;
    }

    public void setLicenseFlateErrorMsg(String licenseFlateErrorMsg) {
        this.licenseFlateErrorMsg = licenseFlateErrorMsg;
    }

    public String getNumberSeatErrorMsg() {
        return numberSeatErrorMsg;
    }

    public void setNumberSeatErrorMsg(String numberSeatErrorMsg) {
        this.numberSeatErrorMsg = numberSeatErrorMsg;
    }


    
}
