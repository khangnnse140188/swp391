/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserValidator;

/**
 *
 * @author khang
 */
public class RentalCarErrorMessage {
    private String licenseFlateErrorMsg;
    private String  priceErrorMsg;
    public RentalCarErrorMessage() {
    }

    public RentalCarErrorMessage(String licenseFlateErrorMsg, String priceErrorMsg) {
        this.licenseFlateErrorMsg = licenseFlateErrorMsg;
        this.priceErrorMsg = priceErrorMsg;
    }



    public String getPriceErrorMsg() {
        return priceErrorMsg;
    }

    public void setPriceErrorMsg(String priceErrorMsg) {
        this.priceErrorMsg = priceErrorMsg;
    }

    public String getLicenseFlateErrorMsg() {
        return licenseFlateErrorMsg;
    }

    public void setLicenseFlateErrorMsg(String licenseFlateErrorMsg) {
        this.licenseFlateErrorMsg = licenseFlateErrorMsg;
    }
    
    
}
