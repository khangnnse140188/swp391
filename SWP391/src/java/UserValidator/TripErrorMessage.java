/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserValidator;

/**
 *
 * @author khang
 */
public class TripErrorMessage {
    private String tripNameError;
    private String startDesError;
    private String endDesError;
    private String startTimeError;
    private String endTimeError;

    public TripErrorMessage() {
    }

    public TripErrorMessage(String tripNameError, String startDesError, String endDesError, String startTimeError, String endTimeError) {
        this.tripNameError = tripNameError;
        this.startDesError = startDesError;
        this.endDesError = endDesError;
        this.startTimeError = startTimeError;
        this.endTimeError = endTimeError;
    }

    


    public String getTripNameError() {
        return tripNameError;
    }

    public void setTripNameError(String tripNameError) {
        this.tripNameError = tripNameError;
    }

    public String getStartDesError() {
        return startDesError;
    }

    public void setStartDesError(String startDesError) {
        this.startDesError = startDesError;
    }

    public String getEndDesError() {
        return endDesError;
    }

    public void setEndDesError(String endDesError) {
        this.endDesError = endDesError;
    }

    public String getEndTimeError() {
        return endTimeError;
    }

    public void setEndTimeError(String endTimeError) {
        this.endTimeError = endTimeError;
    }

    public String getStartTimeError() {
        return startTimeError;
    }

    public void setStartTimeError(String startTimeError) {
        this.startTimeError = startTimeError;
    }
    
    
}
