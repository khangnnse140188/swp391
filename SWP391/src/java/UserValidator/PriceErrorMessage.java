/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserValidator;

/**
 *
 * @author khang
 */
public class PriceErrorMessage {
    String priceErrorMsg;

    public PriceErrorMessage() {
    }

    public PriceErrorMessage(String priceErrorMsg) {
        this.priceErrorMsg = priceErrorMsg;
    }

    public String getPriceErrorMsg() {
        return priceErrorMsg;
    }

    public void setPriceErrorMsg(String priceErrorMsg) {
        this.priceErrorMsg = priceErrorMsg;
    }
    
    
}
