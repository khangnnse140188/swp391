/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class VehicleDTO {
    private int vehicleID;
    private String vehicleName;
    private String licenseFlate;
    private CarTypeDTO carTypeDTO;

    public VehicleDTO() {
    }

    public VehicleDTO(int vehicleID, String vehicleName, String licenseFlate, CarTypeDTO carTypeDTO) {
        this.vehicleID = vehicleID;
        this.vehicleName = vehicleName;
        this.licenseFlate = licenseFlate;
        this.carTypeDTO = carTypeDTO;
    }



    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public CarTypeDTO getCarTypeDTO() {
        return carTypeDTO;
    }

    public void setCarTypeDTO(CarTypeDTO carTypeDTO) {
        this.carTypeDTO = carTypeDTO;
    }

    public String getLicenseFlate() {
        return licenseFlate;
    }

    public void setLicenseFlate(String licenseFlate) {
        this.licenseFlate = licenseFlate;
    }

    public int getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(int vehicleID) {
        this.vehicleID = vehicleID;
    }
    
    
}
