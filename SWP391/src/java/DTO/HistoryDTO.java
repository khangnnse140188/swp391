/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class HistoryDTO {
    private int hisID;
    private UserDTO userDTO;
    private PriceDTO priceDTO;
    private CarRentalDTO carRentalDTO;
    private TicketDTO ticketDTO;

    public HistoryDTO(int hisID, UserDTO userDTO, PriceDTO priceDTO, CarRentalDTO carRentalDTO, TicketDTO ticketDTO) {
        this.hisID = hisID;
        this.userDTO = userDTO;
        this.priceDTO = priceDTO;
        this.carRentalDTO = carRentalDTO;
        this.ticketDTO = ticketDTO;
    }

    public int getHisID() {
        return hisID;
    }

    public void setHisID(int hisID) {
        this.hisID = hisID;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public PriceDTO getPriceDTO() {
        return priceDTO;
    }

    public void setPriceDTO(PriceDTO priceDTO) {
        this.priceDTO = priceDTO;
    }

    public CarRentalDTO getCarRentalDTO() {
        return carRentalDTO;
    }

    public void setCarRentalDTO(CarRentalDTO carRentalDTO) {
        this.carRentalDTO = carRentalDTO;
    }

    public TicketDTO getTicketDTO() {
        return ticketDTO;
    }

    public void setTicketDTO(TicketDTO ticketDTO) {
        this.ticketDTO = ticketDTO;
    }
    
}
