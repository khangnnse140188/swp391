/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class DepositDTO {
    private int id;
    private float deposit;

    public DepositDTO() {
    }

    public DepositDTO(int id, float deposit) {
        this.id = id;
        this.deposit = deposit;
    }

    

    public float getDeposit() {
        return deposit;
    }

    public void setDeposit(float deposit) {
        this.deposit = deposit;
    }
    
     
}
