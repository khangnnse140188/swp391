/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class PriceDTO {
    private int priceID;
    private float price;

    public PriceDTO() {
    }

    public PriceDTO(int priceID, float price) {
        this.priceID = priceID;
        this.price = price;
    }


    public int getPriceID() {
        return priceID;
    }

    public void setPriceID(int priceID) {
        this.priceID = priceID;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
