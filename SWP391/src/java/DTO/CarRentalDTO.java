/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author khang
 */
public class CarRentalDTO {
   private int rentCarID;
   private VehicleDTO vehicleDTO;
   private PriceDTO priceDTO;
   private boolean status;
   private UserDTO userDTO;

    public CarRentalDTO() {
    }

    public CarRentalDTO(int rentCarID, VehicleDTO vehicleDTO, PriceDTO priceDTO,  boolean status, UserDTO userDTO) {
        this.rentCarID = rentCarID;
        this.vehicleDTO = vehicleDTO;
        this.priceDTO = priceDTO;

        this.status = status;
        this.userDTO = userDTO;
    }



    

    public VehicleDTO getVehicleDTO() {
        return vehicleDTO;
    }

    public void setVehicleDTO(VehicleDTO vehicleDTO) {
        this.vehicleDTO = vehicleDTO;
    }

    public PriceDTO getPriceDTO() {
        return priceDTO;
    }

    public void setPriceDTO(PriceDTO priceDTO) {
        this.priceDTO = priceDTO;
    }




    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public int getRentCarID() {
        return rentCarID;
    }

    public void setRentCarID(int rentCarID) {
        this.rentCarID = rentCarID;
    }
   
   
}
