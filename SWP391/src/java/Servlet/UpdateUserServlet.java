/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.UserDAO;
import DTO.DepositDTO;
import DTO.RoleDTO;
import DTO.UserDTO;
import UserValidator.UserErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author KenLy
 */
@WebServlet(name = "UpdateUserServlet", urlPatterns = {"/UpdateUserServlet"})
public class UpdateUserServlet extends HttpServlet {

    private static final String ERROR = "SearchUserServlet";
    private static final String SUCCESS = "SearchUserServlet";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        PrintWriter out = response.getWriter();
        try {
            int userID = Integer.parseInt(request.getParameter("userID"));
            String fullName = request.getParameter("fullName");
            String dob = request.getParameter("dob");
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
            String email = request.getParameter("email");
            String roleName = request.getParameter("roleName");
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            System.out.println(status);
            boolean checkError = true;
            UserErrorMessage userErrorMessage = new UserErrorMessage();
            
            if (!fullName.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                userErrorMessage.setFullnameErrorMsg("Full Name don't contain any special character like @#$");
                checkError = false;
            }
            if (!dob.matches("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$")){
                userErrorMessage.setDobErrorMsg("DOB must match this type yyyy-mm-dd");
                
                checkError = false;
            }
            if (!phone.matches("^\\d{10}$")){
                userErrorMessage.setPhoneErrorMsg("Phone number must contain 10 digit only");
                checkError = false;
            }
            if (!email.matches("[a-z0-9]+@[a-z]+\\.[a-z]{2,3}")){
                checkError = false;
                userErrorMessage.setEmailErrorMsg("Email must be in abc@def.com type");
            }
            UserDAO userDAO = new UserDAO();
            RoleDTO roleDTO = userDAO.getRoleByName(roleName);
            
            if (roleDTO == null ){
                System.out.println("role null");
                checkError = false ;
                userErrorMessage.setRoleErrorMsg("Role must be 1 of 3 type :TC, AD and US");
            }
            if (checkError){
                
            
            UserDTO userDTO = userDAO.getUserByID(userID);
            Date dob1 = Date.valueOf(dob); 
            boolean check = userDAO.UpdateUser(new UserDTO(userID, fullName, dob1, address, phone, email, userDTO.getPassword(), status, userDTO.getDepositDTO(), roleDTO));
            if (check) {


                url = SUCCESS+ "?txtsearch=" + request.getParameter("search");
                }
            }
            else {
                request.setAttribute("Error", userErrorMessage);
                url = ERROR+ "?"
                        + "UpdateID=" + userID
                        + "&txtsearch=" + request.getParameter("search");
                System.out.println(userErrorMessage.getDobErrorMsg());
            }
            
        } catch (SQLException ex) {
            log("SQL Exception : " + ex.getMessage());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateUserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateUserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
