/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.PriceDAO;
import DAO.RentCarDAO;
import DAO.VehicleDAO;
import DTO.CarRentalDTO;
import DTO.CarTypeDTO;
import DTO.PriceDTO;
import DTO.VehicleDTO;
import UserValidator.PriceErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "AddRentalCarServlet", urlPatterns = {"/AddRentalCarServlet"})
public class AddRentalCarServlet extends HttpServlet {
    private final String SUCCESS = "addRentalCar.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = SUCCESS;
        try  {
            boolean checkError = true;
            PriceErrorMessage priceErrorMessage = new PriceErrorMessage();
            String vehicleName = request.getParameter("vehicleName");
            String typeCar = request.getParameter("typeCar");
            String txtPrice = request.getParameter("txtPrice");
            if (!txtPrice.matches("[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)")){
                priceErrorMessage.setPriceErrorMsg("Price must be float type");
                checkError = false;
            }
            if (checkError){
                RentCarDAO rentCarDAO = new RentCarDAO();
                VehicleDAO vehicleDAO = new VehicleDAO();
                float price = Float.parseFloat(txtPrice);
                PriceDAO priceDAO = new PriceDAO();
                int priceID = priceDAO.AddPrice(new PriceDTO(0, price));
                VehicleDTO vehicleDTOName = vehicleDAO.GetVehicleByName(vehicleName);
                rentCarDAO.addRentalCar(new CarRentalDTO(0, vehicleDTOName, priceDAO.getPriceByID(priceID), false, null));
                request.setAttribute("Success", "Add new rental car successfully");
            }
            else {
                request.setAttribute("Error", priceErrorMessage);
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AddRentalCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(AddRentalCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        finally{ 
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
