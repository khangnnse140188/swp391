/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.UserDAO;
import DTO.UserDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "ServletDispatcher", urlPatterns = {"/ServletDispatcher"})
public class ServletDispatcher extends HttpServlet {

    private final String LOGIN_PAGE = "login.jsp";
    private final String LOGIN_CONTROLLER = "LoginServlet";
    private final String SIGNUP_CONTROLLER = "SignUpServlet";
    private final String LOGOUT_CONTROLLER = "LogoutServlet";
    private final String REGISTER_CONTROLLER = "RegisterServlet";
    private final String VIEW_LIST_TRIP_CONTROLLER = "ViewListTripServlet";
    private final String VIEW_LIST_RENTAL_CAR_CONTROLLER = "ViewListRentalCarServlet";
    private final String SEARCH_USER_CONTROLLER = "SearchUserServlet";
    private final String DELETE_USER_CONTROLLER = "DeleteUserServlet";
    private final String UPDATE_USER_CONTROLLER = "UpdateUserServlet";
    private final String SEARCH_VEHICLE_CONTROLLER = "SearchVehicleServlet";
    private final String ADD_TRIP_CONTROLLER_DISPATCHER = "AddTripServletDispatcher";
    private final String ADD_TRIP_CONTROLLER = "AddTripServlet";
    private final String UPDATE_TRIP_CONTROLLER = "UpdateTripServlet";
    private final String DELETE_TRIP_CONTROLLER = "DeleteTripServlet";
    private final String ADD_VEHICLE_CONTROLLER = "AddVehicleServlet";
    private final String UPDATE_VEHICLE_CONTROLLER = "UpdateVehicleServlet";
    private final String DELETE_VEHICLE_CONTROLLER = "DeleteVehicleServlet";
    private final String SEARCH_TRIP_CONTROLLER = "SearchTripServlet";
    private final String SEARCH_RENTALCAR_CONTROLLER = "SearchRentalCarServlet";
    private final String ADD_RENTALCAR_DISPATCHER = "AddRentalCarDispatcherServlet";
    private final String ADD_RENTALCAR_CONTROLLER = "AddRentalCarServlet";
    private final String UPDATE_RENTCAR_CONTROLLER = "UpdateRentalCarServlet";
    private final String DELETE_RENTAL_CONTROLLER = "DeleteRentalCarServlet";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String button = request.getParameter("btnAction");
        String url = LOGIN_PAGE;

        try {
            if ("Login".equals(button)) {
                url = LOGIN_CONTROLLER;
            } else if ("Register".equals(button)) {
                url = SIGNUP_CONTROLLER;
                System.out.println("sign up");
            } else if ("ViewListTrip".equals(button)) {
                url = VIEW_LIST_TRIP_CONTROLLER;
            } else if ("View Rental Car".equals(button)) {
                url = VIEW_LIST_RENTAL_CAR_CONTROLLER;
            } else if ("Search".equals(button)) {
                url = SEARCH_USER_CONTROLLER;
            } else if ("Delete".equals(button)) {
                url = DELETE_USER_CONTROLLER;
            } else if ("Update".equals(button)) {
                url = UPDATE_USER_CONTROLLER;
            } else if ("Search Vehicle".equals(button)) {
                url = SEARCH_VEHICLE_CONTROLLER;
            } else if ("Add Trip".equals(button)) {
                url = ADD_TRIP_CONTROLLER_DISPATCHER;
            } else if ("Create Trip".equals(button)) {
                url = ADD_TRIP_CONTROLLER;
            } else if ("Update Trip".equals(button)) {
                url = UPDATE_TRIP_CONTROLLER;
            } else if ("Delete Trip".equals(button)) {
                url = DELETE_TRIP_CONTROLLER;
            } else if ("CreateVehicle".equals(button)) {
                url = ADD_VEHICLE_CONTROLLER;
            } else if ("Update Vehicle".equals(button)) {
                url = UPDATE_VEHICLE_CONTROLLER;
            } else if ("DeleteVehicle".equals(button)) {
                url = DELETE_VEHICLE_CONTROLLER;
            } else if ("Search Trip".equals(button)) {
                url = SEARCH_TRIP_CONTROLLER;
            }
            
            else if ("Search Rental Car".equals(button)){
                url = SEARCH_RENTALCAR_CONTROLLER;
            }
            
            else if ("Add Rental Car".equals(button)){
                url = ADD_RENTALCAR_DISPATCHER;
            }
            else if ("Create Rental Car".equals(button)){
                url = ADD_RENTALCAR_CONTROLLER;
            }
            
            else if ("Update RentCar".equals(button)){
                System.out.println("a");
                url = UPDATE_RENTCAR_CONTROLLER;
            }
            
            else if ("Delete Rental Car".equals(button)){
                url = DELETE_RENTAL_CONTROLLER;
            }

        } finally {
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
