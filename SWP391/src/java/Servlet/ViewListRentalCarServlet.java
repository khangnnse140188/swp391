/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.RentCarDAO;
import DAO.VehicleDAO;
import DTO.CarRentalDTO;
import DTO.VehicleDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author khang
 */
@WebServlet(name = "ViewListRentalCarServlet", urlPatterns = {"/ViewListRentalCarServlet"})
public class ViewListRentalCarServlet extends HttpServlet {
    private final String GUEST_LIST_RENTAL_CAR = "home.jsp";
    private final String COMPANY_LIST_RENTAL_CAR = "viewRentalCar.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = "";
        try  {
            HttpSession session= request.getSession();
                    
            RentCarDAO rentCarDAO = new RentCarDAO();
            
            ArrayList<CarRentalDTO> listRentalCar = rentCarDAO.getAllRentalCar();
            if (listRentalCar == null){
                System.out.println("list null");
            }
            request.setAttribute("LIST_RENTAL_CAR", listRentalCar);


            if (session.getAttribute("ADMIN") == null && session.getAttribute("TRANSPORTATIONCOMPANY") == null && session.getAttribute("CUSTOMER") == null) {
                url = GUEST_LIST_RENTAL_CAR;
 
            } else if (session.getAttribute("TRANSPORTATIONCOMPANY") != null) {
                VehicleDAO vehicleDAO = new VehicleDAO();
                ArrayList<VehicleDTO> listVehicle = vehicleDAO.getAllVehicle();
                request.setAttribute("ListVehicle", listVehicle);
                url = COMPANY_LIST_RENTAL_CAR;
                
            } 
        }
        catch (ClassNotFoundException ex) {
            log ("Class not found's exception: " + ex.getMessage());
        } catch (SQLException ex) {
            log ("SQL's Exception: " + ex.getMessage());
        }       
        finally{
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
