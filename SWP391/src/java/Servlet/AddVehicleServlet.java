/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.RentCarDAO;
import DAO.VehicleDAO;
import DTO.CarRentalDTO;
import DTO.CarTypeDTO;
import DTO.VehicleDTO;
import UserValidator.VehicleErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author KenLy
 */
@WebServlet(name = "AddVehicleServlet", urlPatterns = {"/AddVehicleServlet"})
public class AddVehicleServlet extends HttpServlet {
    
    private final String SUCCESS = "addVehicle.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        PrintWriter out = response.getWriter();
        String url = SUCCESS;
        try  {
            VehicleErrorMessage vehicleErrorMessage = new VehicleErrorMessage();
            boolean checkError = true;
            String vehicleName = request.getParameter("txtVehicleName");
            String license = request.getParameter("txtLicense");
            String vehicleType = request.getParameter("vehicleType");
            String numberSeat = request.getParameter("numberSeat");
            if (!vehicleName.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                vehicleErrorMessage.setVehicleNameErrorMsg("Vehicle Name does not contain special character");
                checkError = false;
            }
            if (!license.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                vehicleErrorMessage.setLicenseFlateErrorMsg("License Flate does not contain special character");
                checkError = false;
            }
            if (!numberSeat.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                vehicleErrorMessage.setNumberSeatErrorMsg("Number of Seat does not contain special character");
                checkError = false;
            }
            if (checkError){
                VehicleDAO vehicleDAO = new VehicleDAO();
  
                CarTypeDTO carTypeDTO = vehicleDAO.getCarTypeByName(vehicleType);
                VehicleDTO vehicleDTO = new VehicleDTO(0, vehicleName, license, carTypeDTO);
                vehicleDAO.addVehicle(vehicleDTO);
                request.setAttribute("Success", "Add new vehicle successfully");
            }
            else {
                request.setAttribute("Error", vehicleErrorMessage);
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AddVehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(AddVehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        finally{
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
