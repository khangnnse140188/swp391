/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.PriceDAO;
import DAO.TripDAO;
import DAO.VehicleDAO;
import DTO.DestinationDTO;
import DTO.PriceDTO;
import DTO.TripDTO;
import DTO.VehicleDTO;
import UserValidator.PriceErrorMessage;
import UserValidator.TripErrorMessage;
import UserValidator.UserErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "AddTripServlet", urlPatterns = {"/AddTripServlet"})
public class AddTripServlet extends HttpServlet {
    
    private final String SUCCESS = "addTrip.jsp";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = SUCCESS;
        try  {
            TripErrorMessage tripErrorMessage = new TripErrorMessage();
            PriceErrorMessage priceErrorMessage = new PriceErrorMessage();
            String txtTripName = request.getParameter("txtTripName");
            String txtDescription = request.getParameter("txtDescription");
            String txtStartDes = request.getParameter("txtStartDes");
            String txtEndDes = request.getParameter("txtEndDes");
            String startTime = request.getParameter("txtStartTime");
            String[] startSplit = startTime.split("T");
            String startTimeResult = "";
            for (String a : startSplit) {
                startTimeResult+=a+ " ";
            }
            String endTime = request.getParameter("txtArriveTime");
            String[] endSplit = endTime.split("T");
            String endTimeResult = "";
            for (String a : endSplit) {
                endTimeResult+= a+ " ";
            }
            System.out.println(startTimeResult);
            System.out.println(endTimeResult);
            Timestamp txtStartTime = Timestamp.valueOf(startTimeResult);
            Timestamp txtArriveTime = Timestamp.valueOf(endTimeResult);
            
            String carType = request.getParameter("carType");
            String numberSeat = request.getParameter("numberSeat");
            String txtPrice = request.getParameter("txtPrice");
            String vehicleName = request.getParameter("vehicle");
            boolean checkError = true;
            if (!txtTripName.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                tripErrorMessage.setTripNameError("Trip name does not contain special character");
                checkError = false;
            }
            if (!txtStartDes.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                tripErrorMessage.setStartDesError("Start Destination does not contain special character");
                checkError = false;
            }
            if (!txtEndDes.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                tripErrorMessage.setEndDesError("End destination does not contain special character");
                checkError = false;
            }
            if (txtStartTime.after(txtArriveTime)){
                tripErrorMessage.setEndTimeError("End Time must after Start Time");
                checkError = false;
            }
            if (!txtPrice.matches("[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)")){
                priceErrorMessage.setPriceErrorMsg("Price must be float type");
                checkError = false;
            }
            
            if (checkError){
  
                float price1 = Float.parseFloat(txtPrice);
                TripDAO tripDAO = new TripDAO();
                PriceDAO priceDAO = new PriceDAO();
                VehicleDAO vehicleDAO = new VehicleDAO();
                DestinationDTO destinationDTO = new DestinationDTO(0, txtStartDes, txtEndDes, txtStartTime, txtArriveTime);
                VehicleDTO vehicleDTO = vehicleDAO.GetVehicleByName(vehicleName);
                PriceDTO priceDTO = new PriceDTO(0, price1);
                int desID = tripDAO.addDestination(destinationDTO);
                int priceID = priceDAO.AddPrice(priceDTO);
                if (priceID != -1 && desID != -1){
                    PriceDTO priceResult = priceDAO.getPriceByID(priceID);
                    DestinationDTO desResult = tripDAO.getDestinationByID(desID);
                    TripDTO tripDTO = new TripDTO(0, txtTripName, txtDescription, desResult, vehicleDTO, priceResult);
                     tripDAO.addTrip(tripDTO);
                     request.setAttribute("Success", "Trip inserted successfully");
                }
                else {
                    System.out.println("destination check and price check fail");
                }
                

            }
            else {
                
                request.setAttribute("Price_Error", priceErrorMessage);
                request.setAttribute("Trip_Error", tripErrorMessage);
            }
          
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AddTripServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(AddTripServlet.class.getName()).log(Level.SEVERE, null, ex);
        }       
        
        finally{
             url = SUCCESS;
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
