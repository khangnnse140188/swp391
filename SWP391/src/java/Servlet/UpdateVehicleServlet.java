/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.VehicleDAO;
import DTO.CarTypeDTO;
import DTO.VehicleDTO;
import UserValidator.VehicleErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "UpdateVehicleServlet", urlPatterns = {"/UpdateVehicleServlet"})
public class UpdateVehicleServlet extends HttpServlet {
    private final String SUCCESS = "SearchVehicleServlet";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = SUCCESS;
        try  {
            boolean checkError = true;
            int vehicleID = Integer.parseInt(request.getParameter("vehicleID"));
            String search = request.getParameter("search");
            VehicleErrorMessage vehicleErrorMessage = new VehicleErrorMessage();
            String vehicleName = request.getParameter("vehicleName");
            String licenseFlate = request.getParameter("licenseFlate");
            String vehicleType = request.getParameter("vehicleType");
            String numberSeat = request.getParameter("numberOfSeat");
            if (!vehicleName.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                vehicleErrorMessage.setVehicleNameErrorMsg("Vehicle Name does not contain special character");
                checkError = false;
            }
            if (!licenseFlate.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                vehicleErrorMessage.setLicenseFlateErrorMsg("License Flate does not contain special character");
                checkError = false;
            }
            if (checkError){
                
                VehicleDAO vehicleDAO = new VehicleDAO();
                CarTypeDTO carTypeDTO = vehicleDAO.getCarTypeByName(vehicleType);
                boolean check = vehicleDAO.updateVehicle(new VehicleDTO(vehicleID, vehicleName, licenseFlate, carTypeDTO));
                
                url = SUCCESS + "?"
                        + "txtsearchVehicle=" + search;
            }
            else {
                url = SUCCESS + "?"
                        + "txtsearchVehicle=" + search
                        + "&UpdateID=" + vehicleID;
                request.setAttribute("Error", vehicleErrorMessage);
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateVehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UpdateVehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        finally{
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
