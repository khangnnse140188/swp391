/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.UserDAO;
import DAO.VehicleDAO;
import DTO.UserDTO;
import DTO.VehicleDTO;
import UserValidator.UserErrorMessage;
import UserValidator.VehicleErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author KenLy
 */
@WebServlet(name = "SearchVehicleServlet", urlPatterns = {"/SearchVehicleServlet"})
public class SearchVehicleServlet extends HttpServlet {

    private static final String ERROR = "pageCompany.jsp";
    private static final String COMPANY = "pageCompany.jsp";
    private static final String HOME = "home.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        PrintWriter out = response.getWriter();
        try {
            HttpSession session = request.getSession();
            
            
            VehicleErrorMessage vehicleErrorMessage =  (VehicleErrorMessage) request.getAttribute("Error");
            String updateID = request.getParameter("UpdateID");
            String search = request.getParameter("txtsearchVehicle");
            if (search == null) {
                search = "";
            }
            VehicleDAO vehicleDAO = new VehicleDAO();

            ArrayList<VehicleDTO> listVehicle = vehicleDAO.searchVehicle(search);

            if (!listVehicle.isEmpty()) {

                request.setAttribute("VehicleList", listVehicle);
                if (updateID != null) {
                    request.setAttribute("UpdateID", updateID);
                }
                if (vehicleErrorMessage != null) {
                    request.setAttribute("Error", vehicleErrorMessage);
                }
            }
            
            if (session.getAttribute("ADMIN") == null && session.getAttribute("TRANSPORTATIONCOMPANY") == null && session.getAttribute("CUSTOMER") == null){
                url = HOME;
            }
            else if (session.getAttribute("TRANSPORTATIONCOMPANY") != null){
                url = COMPANY;
            }
        } catch (SQLException ex) {
            log("SQL Exception : " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SearchVehicleServlet.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
