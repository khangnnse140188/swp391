/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.PriceDAO;
import DAO.RentCarDAO;
import DAO.VehicleDAO;
import DTO.CarRentalDTO;
import DTO.PriceDTO;
import DTO.VehicleDTO;
import UserValidator.RentalCarErrorMessage;
import UserValidator.VehicleErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "UpdateRentalCarServlet", urlPatterns = {"/UpdateRentalCarServlet"})
public class UpdateRentalCarServlet extends HttpServlet {
    private final String SUCCESS = "SearchRentalCarServlet";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = SUCCESS;
        try  {
            boolean checkError = true;
            RentalCarErrorMessage rentalCarErrorMessage = new RentalCarErrorMessage();
            String search = request.getParameter("search");
            int rentID = Integer.parseInt(request.getParameter("rentID"));
            String vehicleName = request.getParameter("vehicleName");

            String license = request.getParameter("license");
            System.out.println(license);
            String price = request.getParameter("price");
            if (!license.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                rentalCarErrorMessage.setLicenseFlateErrorMsg("License Flate does not contain special character");
                checkError = false;
                
            }
            if (!price.matches("[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)")){
                rentalCarErrorMessage.setPriceErrorMsg("Price must be float type");
                checkError = false;
                
            }
            if (checkError){
                Float priceresult = Float.parseFloat(price);
                RentCarDAO rentCarDAO = new RentCarDAO();
                VehicleDAO vehicleDAO = new VehicleDAO();
                PriceDAO priceDAO = new PriceDAO();
                priceDAO.UpdatePrice(new PriceDTO(rentCarDAO.getRentCarByID(rentID).getPriceDTO().getPriceID(), priceresult));

                CarRentalDTO carRentalDTO = rentCarDAO.getRentCarByID(rentID);
                vehicleDAO.updateVehicle(new VehicleDTO(carRentalDTO.getVehicleDTO().getVehicleID(), carRentalDTO.getVehicleDTO().getVehicleName(), license, carRentalDTO.getVehicleDTO().getCarTypeDTO()));
                CarRentalDTO carRentalDTOUpdate = rentCarDAO.getRentCarByID(rentID);
                rentCarDAO.updateRentalCar(new CarRentalDTO(rentID, carRentalDTOUpdate.getVehicleDTO(), rentCarDAO.getRentCarByID(rentID).getPriceDTO(), checkError, rentCarDAO.getRentCarByID(rentID).getUserDTO()));
                url = SUCCESS + "?"
                        + "txtsearchRentalCar="+search;
                System.out.println(rentCarDAO.getRentCarByID(rentID).getVehicleDTO().getLicenseFlate());
            }
            else {
                url = SUCCESS + "?"
                        + "txtsearchRentalCar=" + search
                        + "&UpdateID=" + rentID;
                request.setAttribute("Error", rentalCarErrorMessage);
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateRentalCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UpdateRentalCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        }        finally{
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
