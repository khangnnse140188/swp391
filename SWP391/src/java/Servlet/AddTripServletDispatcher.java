/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.TripDAO;
import DAO.VehicleDAO;
import DTO.TripDTO;
import DTO.VehicleDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "AddTripServletDispatcher", urlPatterns = {"/AddTripServletDispatcher"})
public class AddTripServletDispatcher extends HttpServlet {
    private final String ADD_TRIP = "addTrip.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = "";
        try  {
            VehicleDAO vehicleDAO = new VehicleDAO();
            ArrayList<VehicleDTO> vehicleList = vehicleDAO.getAllVehicle();
            TripDAO tripDAO = new TripDAO();
            ArrayList<TripDTO> tripList = tripDAO.getAllTrip();
            ArrayList<VehicleDTO> vehicleRemoveList = new ArrayList<>();
            for (TripDTO tripDTO : tripList) {
                for (VehicleDTO vehicleDTO : vehicleList) {
                    if (tripDTO.getVehicleDTO().getVehicleID() == vehicleDTO.getVehicleID()){
                        vehicleRemoveList.add(vehicleDTO);
                    }
                }
            }
            vehicleList.removeAll(vehicleRemoveList);
            for (VehicleDTO vehicleDTO : vehicleList) {
                System.out.println(vehicleDTO.getVehicleName());
            }
            if (!vehicleList.isEmpty()){
                ServletContext sc = getServletContext();
                sc.setAttribute("Vehicle_List_For_Trip", vehicleList);
            }
            else {
                request.setAttribute("Out_Of_Vehicle", "All Vehicle are not alvailable please insert more vehicle to create trip");
            }
            
            
            
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AddTripServletDispatcher.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(AddTripServletDispatcher.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        finally{
            url = ADD_TRIP;
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
