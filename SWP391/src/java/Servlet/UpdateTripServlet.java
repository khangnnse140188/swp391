/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.TripDAO;
import DTO.DestinationDTO;
import DTO.TripDTO;
import UserValidator.TripErrorMessage;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author khang
 */
@WebServlet(name = "UpdateTripServlet", urlPatterns = {"/UpdateTripServlet"})
public class UpdateTripServlet extends HttpServlet {
    private final String SUCCESS = "SearchTripServlet";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String url = "";
        try  {
            String searchValue = request.getParameter("txtsearchTrip");
            if (searchValue == null){
                searchValue = "";
            }
            TripErrorMessage tripErrorMessage = new TripErrorMessage();
            boolean checkError = true;
            int tripID = Integer.parseInt(request.getParameter("tripID"));
            String tripName = request.getParameter("tripName");
            String description = request.getParameter("description");
            String startDes = request.getParameter("startDes");
            String endDes = request.getParameter("endDes");
            String startTime = request.getParameter("startTime");
            String arriveTime = request.getParameter("ArriveTime");
            String regexTimeStamp = "((((19|20)([2468][048]|[13579][26]|0[48])|2000)-02-29|((19|20)[0-9]{2}-(0[4678]|1[02])-(0[1-9]|[12][0-9]|30)|(19|20)[0-9]{2}-(0[1359]|11)-(0[1-9]|[12][0-9]|3[01])|(19|20)[0-9]{2}-02-(0[1-9]|1[0-9]|2[0-8])))\\s([01][0-9]|2[0-3]):([012345][0-9]):([012345][0-9]))";
            if (!tripName.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                tripErrorMessage.setTripNameError("Trip name does not contain special character");
                checkError = false;
            }
            if (!startDes.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                tripErrorMessage.setStartDesError("Start destination does not contain special character");
                checkError = false;
            }
            if (!endDes.matches("^[_A-z0-9]*((-|\\s)*[_A-z0-9])*$")){
                tripErrorMessage.setEndDesError("End destination does not contain special character");
                checkError = false;
            }
//            if (!startTime.matches(regexTimeStamp)){
//                tripErrorMessage.setStartTimeError("Start time must have this format :yyyy-mm-dd hh:mm:ss");
//                checkError = false;
//            }
//            if (!arriveTime.matches(regexTimeStamp)){
//                tripErrorMessage.setEndTimeError("Arrive time must have this format :yyyy-mm-dd hh:mm:ss");
//                checkError = false;
//            }
            if (checkError){
                Timestamp startTimeResult = Timestamp.valueOf(startTime);
                Timestamp arriveTimeResult = Timestamp.valueOf(arriveTime);
                TripDAO tripDAO = new TripDAO();
                TripDTO tripDTODes = tripDAO.getTripById(tripID);
                int desIDUpdate = tripDTODes.getDestinationDTO().getDesId();
                int desID = tripDAO.updateDestination(new DestinationDTO(tripID, startDes, endDes, startTimeResult, arriveTimeResult));
                DestinationDTO desFinal  = tripDAO.getDestinationByID(desIDUpdate);
                TripDTO tripDTO = new TripDTO(tripID, tripName, description, desFinal, tripDTODes.getVehicleDTO(), tripDTODes.getPriceDTO());
                boolean checkTrip = tripDAO.updateTrip(tripDTO);
                url = SUCCESS + "?"
                        + "txtsearchTrip=" + searchValue;
            }
            else {
                url = SUCCESS + "?"
                        + "txtsearchTrip=" + searchValue
                        + "&UpdateID=" + tripID;
                request.setAttribute("tripErrorMessage", tripErrorMessage);
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(UpdateTripServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UpdateTripServlet.class.getName()).log(Level.SEVERE, null, ex);
        }       
        finally{
            request.getRequestDispatcher(url).forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
