use SWP391_Database
select vehicleID,vehicleName,licensePlates,carTypeID from tblVehicle where vehicleID = ?

select startDes,endDes,startTime,arriveTime from tblDestination where desID = ?

select carTypeID,carTypeName,numberOfSeat from tblCarType where carTypeID = ?

select priceID,price,amount,TypeID,totalPrice  from tblPrice where priceID = ?

select TypeID,TypeName from tblTypeOfPrice where TypeID = ?

select licenseID, driverID, Class,ClassificationOfMotorVehicles,BeginningDate, Expires  from tblLicenseVehicle where licenseID = ? 

select driverID,driverName,dob,address,nationality from tblDriver where driverID = ?

select userID,fullName,dob,address,phone,email,wallet,password,roleID from tblUsers where phone = ? and password = ?

insert into tblUsers values (?,?,?,?,?,?,?,1)

insert into tblDeposit values (0)

select @@IDENTITY as depositID
delete from tblDeposit where depositID = 9 

delete from tblDeposit where deposit = 0

DBCC CHECKIDENT ('tblUsers')

DBCC checkident ('tblUsers',RESEED,8)

delete from tblUsers where userID > 8

select tripID,tripName,description,desID from tblTrip where tripName like ?

select rentCarID,vehicleID,priceID,licenseID,Status,user from tblCarRental where 

select rentCarID,vehicleID,priceID,licenseID,Status,userID from tblCarRental where  like ?

insert into tblTrip values (?,?,?)

use SWP391_Database
select status from tblUsers

select vehicleID,vehicleName,licensePlates,carTypeID from tblVehicle

select tripID, tripName, description, desID, vehicleID from tblTrip

update tblTrip set tripName = ?,description = ?,desID = ? , vehicleID = ?  where tripID = ?

delete from tblTrip where tripID = ?
select * from tblDestination
insert into tblDestination values (?,?,?,?)

update tblDestination set startDes = ?, endDes = ?,startTime = ?,arriveTime = ? where desID = ?

delete from tblDestination where desID = ?
select * from tblVehicle
insert into tblVehicle values (?,?,?)

update tblVehicle set vehicleName = ?, licensePlates = ? , carTypeID = ? where vehicleID = ?

delete from tblVehicle where vehicleID = ?
select * from tblCarRental
insert into tblCarRental values (?,?,?,?,?)

update tblCarRental set vehicleID = ?, priceID = ?, licenseID = ?, Status = ? , userID = ? where rentCarID = ?

delete from tblCarRental where rentCarID = ?
select * from tblDriver
insert into tblDriver values (?,?,?,?)

update tblDriver set driverName = ?, dob = ?, address = ?, nationality = ? where driverID = ?

delete from tblDriver where driverID = ?
select * from tblLicenseVehicle
insert into tblLicenseVehicle values (?,?,?,?,?)

update tblLicenseVehicle set driverID = ? ,Class = ? , ClassificationOfMotorVehicles = ?, BeginningDate = ?, Expires = ? where licenseID = ?

delete from tblLicenseVehicle where licenseID = ?

select priceID,price from tblPrice

insert into tblPrice values (?)

update tblPrice set price = ? where priceID = ?

delete from tblPrice where priceID = ?

select tripID,tripName,description,priceID,desID,vehicleID from tblTrip
select vehicleID,vehicleName,licensePlates,carTypeID from tblVehicle where vehicleName like ?
use SWP391_Database

delete from tblTrip where tripID = 11

select rentCarID,vehicleID,priceID,Status,userID from tblCarRental where vehicleID = ?

use SW