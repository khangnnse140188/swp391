﻿--Sử dụng Table
USE SWP391_Database;
GO

--execute từng Table
---thêm role
GO
INSERT INTO tblRoles(roleName)
VALUES (N'US'), (N'AD'), (N'TC')

---Thêm các loại phương tiện
GO
INSERT INTO tblCarType(carTypeName, numberOfSeat)
VALUES (N'4 seater car', 4), (N'7 seater car', 7), (N'16 seater car', 16), (N'32 seater car', 32)

---Thêm danh sách số tiền được thêm vào
GO
INSERT INTO tblDeposit(deposit)
VALUES (1000000.0), (2000000.0), (3000000.0), (4000000.0), 
       (5000000.0), (6000000.0), (7000000.0), (8000000.0)

---Thêm thông tin giá
GO 
INSERT INTO tblPrice(Price)
VALUES (150000.0), (100000.0), 
	   (200000.0), (130000.0)

---Thêm vị trí chỗ ngồi
GO
INSERT INTO tblPosSeat(seatPosName)
VALUES (N'Seat 1'), (N'Seat 2'), (N'Seat 3'), (N'Seat 4'), (N'Seat 5'), (N'Seat 6'), 
       (N'Seat 7'), (N'Seat 8'), (N'Seat 9'), (N'Seat 10'), (N'Seat 11'), (N'Seat 12'), 
	   (N'Seat 13'), (N'Seat 14'), (N'Seat 15'), (N'Seat 16'), (N'Seat 17'), (N'Seat 18'),
	   (N'Seat 19'), (N'Seat 20'), (N'Seat 21'), (N'Seat 22'), (N'Seat 23'), (N'Seat 24'),
	   (N'Seat 25'), (N'Seat 26'), (N'Seat 27'), (N'Seat 28'), (N'Seat 29'), (N'Seat 30'),
	   (N'Seat 31'), (N'Seat 32')

---Thêm địa điểm và thời gian chuyến đi
GO
INSERT INTO tblDestination(startDes, endDes, startTime, arriveTime)
VALUES (N'HCM', N'Da Nang', N'2022-10-01 06:00:00', N'2022-10-01 22:00:00'),
	   (N'HCM', N'Can Tho', N'2022-10-01 06:00:00', N'2022-10-01 10:00:00'),
	   (N'HCM', N'Nha Trang', N'2022-10-01 06:00:00', N'2022-10-01 18:00:00'),
	   (N'HCM', N'Phu Quoc', N'2022-10-01 06:00:00', N'2022-10-01 20:00:00'),
	   (N'HCM', N'Binh Duong', N'2022-10-01 06:00:00', N'2022-10-01 09:00:00'),
	   (N'HCM', N'Vinh', N'2022-10-01 06:00:00', N'2022-10-03 06:00:00'),
	   (N'HCM', N'Ha Noi', N'2022-10-01 06:00:00', N'2022-10-05 22:00:00')

---Thêm thông tin người dùng + role
GO
INSERT INTO tblUsers(fullName, dob, address, phone, email, wallet, password, status, roleID)
VALUES (N'Hung', '2000-01-02', '123 ABC', 1111111111, N'abc@gmail.com', 1, 1, 1, 2),
       (N'Hoang', '2001-12-02', '123 ABC', 2222222222, N'abc1@gmail.com', 2, 1, 1, 3),
	   (N'Khang', '2000-02-02', '123 ABC', 3333333333, N'abc2@gmail.com', 3, 1, 1, 2),
	   (N'Thinh', '2002-02-02', '123 ABC', 4444444444, N'abc3@gmail.com', 4, 1, 1, 3),
	   (N'Nhan', '1999-02-02', '123 ABC', 5555555555, N'abc4@gmail.com', 5, 1, 1, 1),
	   (N'Dat', '1998-02-02', '123 ABC', 6666666666, N'abc5@gmail.com', 6, 1, 1, 1),
	   (N'Vu', '1997-02-02', '123 ABC', 7777777777, N'abc6@gmail.com', 7, 1, 1, 1),
	   (N'Dung', '1996-02-02', '123 ABC', 8888888888, N'abc7@gmail.com', 8, 1, 1, 1)

---Thêm tài xế
GO


INSERT INTO tblVehicle(vehicleName, licensePlates, carTypeID)
VALUES (N'KIA', N'55A-11111', 1), (N'Hyundai', N'55A-22222', 2),
	   (N'Toyota', N'55A-33333', 1), (N'Honda', N'55A-44444', 2),
	   (N'KIA', N'55A-55555', 1), (N'Hyundai', N'55A-66666', 2), 
	   (N'Toyota', N'55A-77777', 1), (N'Honda', N'55A-88888', 2),
	   (N'KIA', N'50A-11111', 3), (N'Hyundai', N'50A-22222', 3),
	   (N'Toyota', N'50A-33333', 3), (N'Honda', N'50A-44444', 3),
	   (N'KIA', N'50A-55555', 4), (N'Hyundai', N'50A-66666', 4), 
	   (N'Toyota', N'50A-77777', 4), (N'Honda', N'50A-88888', 4)

---Thêm thông tin chuyến đi
GO
INSERT INTO tblTrip(tripName, description, priceID, desID, vehicleID)
VALUES (N'HCM to Da Nang', N'abc', 1, 1, 9),
	   (N'HCM to Can Tho', N'abc', 2, 2, 10),
	   (N'HCM to Nha Trang', N'abc', 3, 3, 11),
	   (N'HCM to Phu Quoc', N'abc', 4, 4, 12),
	   (N'HCM to Binh Duong', N'abc', 1, 5, 13),
	   (N'HCM to Vinh', N'abc', 2, 6, 14),
	   (N'HCM to Ha Noi', N'abc', 3, 7, 15)

---Thêm người dùng thuê xe + giá xe (mệnh giá VND)
GO
INSERT INTO tblCarRental(vehicleID, priceID, Status, userID)
VALUES (1, 1,  1, 5), (5, 2,  0, 6), 
	   (2, 3,  1, 7), (6, 4,  1, 8)

---Thêm người dùng mua vé + giá vé (mệnh giá VND)
GO
INSERT INTO tblTicket(tripID, seatPosID, userID)
VALUES (1, 1, 5),
	   (2, 2, 7)

---Thêm danh sách giá theo loại xe
GO 
INSERT INTO tblHistory(userID, priceID, rentCarID, ticketID)
VALUES (1, 1, 1, 1), (2, 2, 2, 2)