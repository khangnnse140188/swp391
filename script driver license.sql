﻿---tạo thông tin bằng lái của chủ xe
--CREATE TABLE [tblLicenseVehicle](
	--[licenseID] INT IDENTITY(1,1) PRIMARY KEY,
	--[driverID] INT FOREIGN KEY REFERENCES [tblDriver](driverID),
	--[Class] [nvarchar] (3),
	--[ClassificationOfMotorVehicles] [nvarchar] (1000),
	--[BeginningDate] [DATE],
	--[Expires] [DATE]
--)

---tạo danh sách các tài xế
--CREATE TABLE [tblDriver](
	--[driverID] INT IDENTITY(1,1) PRIMARY KEY,
	--[driverName] [nvarchar] (100),
	--[dob] [DATE],
	--[address] [nvarchar] (50),
	--[nationality] [nvarchar] (50),
--)

INSERT INTO tblLicenseVehicle(driverID, Class, ClassificationOfMotorVehicles, BeginningDate, Expires)
VALUES (1, N'B11', N'Motor vehicle with automatic transmission, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-01-02', '2035-01-02'),

	   (2, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2026-12-02', '2036-12-02'),

	   (3, N'B12', N'Motor vehicle used for the carriage of passengers, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-02-02', '2035-02-02'),

	   (4, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2022-02-02', '2037-02-02'),

	   (5, N'B11', N'Motor vehicle with automatic transmission, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-02-02', '2044-02-02' ),

	   (6, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2022-02-02', '2043-01-02'),

	   (7, N'B12', N'Motor vehicle used for the carriage of passengers, 
		having a permisbile maximun mass not exceeding 3500kgs and 
		not more than 9 seats; not used for commercial purpose', '2022-02-02', '2042-02-02'),

	   (8, N'B2', N'Truck, tractor with a trailer exceeding 3500kgs and vehicles classes B1, B2', '2022-02-02', '2041-01-02')

---Thêm phương tiện du lịch + thông tin chỗ ngồi
GO


INSERT INTO tblDriver(driverName, dob, address, nationality)
VALUES (N'Hung', '1980-01-02', '456 ABC', N'Viet Nam'),
       (N'Hoang', '1981-12-02', '456 ABC', N'Viet Nam'),
	   (N'Khang', '1980-02-02', '456 ABC', N'Viet Nam'),
	   (N'Thinh', '1982-02-02', '456 ABC', N'Viet Nam'),
	   (N'Nhan', '1989-02-02', '456 ABC', N'Viet Nam'),
	   (N'Dat', '1988-02-02', '456 ABC', N'Viet Nam'),
	   (N'Vu', '1987-02-02', '456 ABC', N'Viet Nam'),
	   (N'Dung', '1986-02-02', '456 ABC', N'Viet Nam')

---Thêm thông tin bằng lái cho tài xế
GO